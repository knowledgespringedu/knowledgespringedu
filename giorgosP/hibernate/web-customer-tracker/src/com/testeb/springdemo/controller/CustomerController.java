package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.testeb.springdemo.dao.CustomerDAO;
import com.testeb.springdemo.entity.Customer;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	//need to inject the DAO 
	@Autowired
	private CustomerDAO customerDAO;
	
	
	
	@RequestMapping("/list")
	public String showList(Model theModel){
		
		//get customers from DAO
		List<Customer> theCustomers = customerDAO.getCustomers();
		
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		
		return "list-customer";
	}
	
	@RequestMapping("/create")
	public String create(Model theModel){
		Customer customer = new Customer();
		theModel.addAttribute(customer);
		return "create-customer";
	}
	
	@RequestMapping("/createCustomer")
	public String createCustomer(@ModelAttribute("customer") Customer customer, Model theModel){
		customerDAO.createCustomer(customer);
		
		//get customers from DAO
		List<Customer> theCustomers = customerDAO.getCustomers();
		
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		theModel.addAttribute("message","new customer created");
		
		return "list-customer";
	}
	
	@RequestMapping("/update")
	public String update(@RequestParam("id") int id, Model theModel){
		Customer customer = customerDAO.getCustomer(id);
		
		System.out.println(customer.toString());
		theModel.addAttribute(customer);
		return "update-customer";
	}
	
	@RequestMapping("/updateCustomer")
	public String updateCustomer(@ModelAttribute("customer") Customer customer, Model theModel){
		
		System.out.println(customer.toString());
		
		customerDAO.updateCustomer(customer.getId(), customer.getFirstName(), customer.getLastName(), customer.getEmail());
		
		//get customers from DAO
		List<Customer> theCustomers = customerDAO.getCustomers();
		
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		theModel.addAttribute("message","customer updated");
		
		return "list-customer";
	}
	
	@RequestMapping("/delete")
	public String delete(@RequestParam("id") int id, Model theModel){
		customerDAO.deleteCustomer(id);
		
		//get customers from DAO
		List<Customer> theCustomers = customerDAO.getCustomers();
		
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		theModel.addAttribute("message","customer deleted");
		
		return "list-customer";
	}
}
