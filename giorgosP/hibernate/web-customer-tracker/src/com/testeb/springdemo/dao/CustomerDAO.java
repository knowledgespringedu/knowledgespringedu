package com.testeb.springdemo.dao;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerDAO {

	public List<Customer> getCustomers();
	public Customer getCustomer(int id);
	public void createCustomer(Customer customer);
	public void deleteCustomer(int id);
	public void updateCustomer(int id, String firstName, String lastName, String email);
}
