<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<body>
	<form:form action="updateCustomer" modelAttribute="customer">
		First Name: <form:input path="firstName"/>
		<br>
		Last Name: <form:input path="lastName"/>
		<br>
		Email: <form:input path="email"/>
		<br>
		<form:hidden path="id" />
		
		<input type="submit" value="Submit"/>
		
	</form:form>
</body>

</html>