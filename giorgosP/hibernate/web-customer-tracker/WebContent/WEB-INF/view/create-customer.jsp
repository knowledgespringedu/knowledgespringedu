<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<body>
	<form:form action="createCustomer" modelAttribute="customer">
		First Name: <form:input path="firstName"/>
		<br>
		Last Name: <form:input path="lastName"/>
		<br>
		Email: <form:input path="email"/>
		<br>
		<input type="submit" value="Submit"/>
	</form:form>
</body>

</html>