<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>

<html>

<head>
	<title>  List Customers</title>
	<link type="text/css"
		rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/style.css">
		
</head>
<body>
	<div id="wrapper">
		<div id="header">
		
			<h2>CRM - Customer Relationship Manager</h2>
		</div>
		
	</div>
	<div id="container">
	
		<table>
			<tr>
				
			</tr>
			<tr>
				<th><a href="create">Create New Customer</a></th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th></th>
				<th></th>
			</tr>
			
			<!--  loop over and print our customers -->
			<c:forEach var="tempCustomer" items="${customers}">
				<tr>
					<td></td>
					<td> ${tempCustomer.firstName} </td>
					<td> ${tempCustomer.lastName} </td>
					<td> ${tempCustomer.email} </td>
					<td> <a href="update?id=${tempCustomer.id}">Update</a> </td>
					<td> <a href="delete?id=${tempCustomer.id}">Delete</a> </td>
				</tr>
			</c:forEach>
		
		</table>
	
		<c:if test="${message}">
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			   ${message}
			</div>
		</c:if>
	
	</div>
</body>
</html>