package com.testeb.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	//need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public List<Customer> getCustomers() {

		//get the current hibernate session
		Session currentSession=sessionFactory.getCurrentSession();
		
		//create a query
		Query<Customer> theQuery =currentSession.createQuery("from Customer",Customer.class);
		
		//execute query and get result list
		List<Customer> customers=theQuery.getResultList();
		
		//return the results
		return customers;
	}

	@Override
	@Transactional
	public void createCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
		//get the current hibernate session
		Session currentSession=sessionFactory.getCurrentSession();
		
		//save customer
		 currentSession.save(customer);
	}

	@Override
	@Transactional
	public void deleteCustomer(int id) {
		// TODO Auto-generated method stub
		
		//get the current hibernate session
		Session currentSession=sessionFactory.getCurrentSession();
		
		//delete customer
		 Customer myCustomer = currentSession.get(Customer.class, id);
		 currentSession.delete(myCustomer);
	}

	@Override
	@Transactional
	public void updateCustomer(int id, String firstName, String lastName, String email) {
		 // TODO Auto-generated method stub
		
		 //get the current hibernate session
		 Session currentSession=sessionFactory.getCurrentSession();
		
		 //delete customer
		 Customer myCustomer = currentSession.get(Customer.class, id);
		 myCustomer.setFirstName(firstName);
		 myCustomer.setLastName(lastName);
		 myCustomer.setEmail(email);
		
	}

	@Override
	@Transactional
	public Customer getCustomer(int id) {
		// TODO Auto-generated method stub
		//get the current hibernate session
		Session currentSession=sessionFactory.getCurrentSession();
		
		//delete customer
		Customer myCustomer = currentSession.get(Customer.class, id);
		return myCustomer;
	}

}
