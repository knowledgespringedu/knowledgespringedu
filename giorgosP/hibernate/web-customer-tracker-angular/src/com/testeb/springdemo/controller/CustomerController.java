package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.testeb.springdemo.dao.CustomerDAO;
import com.testeb.springdemo.entity.Customer;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	//need to inject the DAO 
	@Autowired
	private CustomerDAO customerDAO;
	
	
	//LIST CUSTOMERS
	@GetMapping(value="/listJSON")
	public @ResponseBody List<Customer> getCustomers(){
		List<Customer> theCustomers = customerDAO.getCustomers();
		return theCustomers;
	}
	
	//CREATE NEW CUSTOMER
	//@CrossOrigin(origins = "http://localhost:8080")
	@PostMapping(value = "/createJSON")
	public ResponseEntity<List<Customer>> createCustomer(@RequestBody Customer customer) {
		customerDAO.createCustomer(customer);
		List<Customer> theCustomers = customerDAO.getCustomers();
		return new ResponseEntity<List<Customer>>(theCustomers, HttpStatus.OK);
	}
	
	
	//UPDATE CUSTOMER
	@CrossOrigin(origins = "http://localhost:8080")
	@PutMapping("/updateJSON")
	public  ResponseEntity<List<Customer>> updateCustomer(@RequestBody Customer customer) {
		customerDAO.updateCustomer(customer.getId(), customer.getFirstName(), customer.getLastName(), customer.getEmail());
		
		List<Customer> theCustomers = customerDAO.getCustomers();
		return new ResponseEntity<List<Customer>>(theCustomers, HttpStatus.OK);
	}
	
	

	
	@DeleteMapping("/deleteJSON/{id}")
	public  ResponseEntity<List<Customer>> deleteCustomer(@PathVariable int id) {
		//System.out.println(id);
		customerDAO.deleteCustomer(id);
		
		List<Customer> theCustomers = customerDAO.getCustomers();
		return new ResponseEntity<List<Customer>>(theCustomers, HttpStatus.OK);
	}
}
