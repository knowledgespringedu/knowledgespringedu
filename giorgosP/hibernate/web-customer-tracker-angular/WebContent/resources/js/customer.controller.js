/**
 * 
 */
(function(){
	'use strict';
	angular.module('app')
		   .controller('customerController', customerController);
	
	customerController.$inject = ['$http'];
	
	function customerController($http){
		var vm = this;
		
		vm.list = list;
		vm.create = create;
		vm.update = update;
		vm.updateCustomer = updateCustomer;
		vm.confirmDelete = confirmDelete;

		vm.customers = [];
		
		list();
		
		function list(){
			$http({
			  method: 'GET',
			  url: 'http://localhost:8080/web-customer-tracker-angular/customer/listJSON'
			}).then(function(res) {
				console.log(res);
				vm.customers = res.data;
			}).catch(function(err) {
				console.log(err);
			});
		}
		
		function create(){
			$http({
			  method: 'POST',
			  url: 'http://localhost:8080/web-customer-tracker-angular/customer/createJSON',
			  data: {
				  firstName: vm.firstName,
				  lastName: vm.lastName,
				  email: vm.email
			  }
			}).then(function(res) {
				console.log(res);
				vm.customers = res.data;
			}).catch(function(err) {
				console.log(err);
			});
		}
		
		function update(index){
			//vm.customerForm.$setPristine();
			
			var customer = vm.customers[index];
			vm.id = customer.id;
			vm.firstName = customer.firstName;
			vm.lastName = customer.lastName;
			vm.email = customer.email;
		}
		
		function updateCustomer(id){
			$http({
				  method: 'PUT',
				  url: 'http://localhost:8080/web-customer-tracker-angular/customer/updateJSON',
				  data: {
					  id: vm.id,
					  firstName: vm.firstName,
					  lastName: vm.lastName,
					  email: vm.email
				  }
				}).then(function(res) {
					console.log(res);
					vm.customers = res.data;
				}).catch(function(err) {
					console.log(err);
				});
		}
		
		function updateCustomer(id){
			$http({
				  method: 'PUT',
				  url: 'http://localhost:8080/web-customer-tracker-angular/customer/updateJSON',
				  data: {
					  id: vm.id,
					  firstName: vm.firstName,
					  lastName: vm.lastName,
					  email: vm.email
				  }
				}).then(function(res) {
					console.log(res);
					vm.customers = res.data;
				}).catch(function(err) {
					console.log(err);
				});
		}
		
		function confirmDelete(id){
			if (confirm("Are you sure?")) {
				deleteCustomer(id);
			}
		}
		
		function deleteCustomer(id){
				$http({
				  method: 'DELETE',
				  url: 'http://localhost:8080/web-customer-tracker-angular/customer/deleteJSON/'+id
				}).then(function(res) {
					console.log(res);
					vm.customers = res.data;
				}).catch(function(err) {
					console.log(err);
				});
		}
		
	}
	
})();