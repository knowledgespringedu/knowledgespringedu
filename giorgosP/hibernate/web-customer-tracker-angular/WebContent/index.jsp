<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html ng-app="app" ng-cloak>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>


<head>
    <title>AngularJS List Customers</title>
   
</head>
<body>
<div class="container">

	<div id="wrapper">
		<div id="header">
		
			<h2>AngularJS - Customer Relationship Manager</h2>
		</div>
		
	</div>
	<div id="container" ng-controller="customerController as vm">
	
	
		<form name="customerForm" role="form">
				First Name: <input type="text" ng-model="vm.firstName"/>
			<br>
			Last Name: <input type="text" ng-model="vm.lastName"/>
			<br>
			Email: <input type="email" ng-model="vm.email"/>
			<br>
			<input type="submit" ng-click="vm.updateCustomer(vm.id)" value="Submit"/>
		</form>
		
	
	
		<table>
			<tr>
				
			</tr>
			<tr>
				<th ng-click="vm.create()"><a>Create New Customer</a></th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th></th>
				<th></th>
			</tr>
			<tr ng-repeat="tempCustomer in vm.customers track by $index">
				<td></td>
				<td> {{tempCustomer.firstName}} </td>
				<td> {{tempCustomer.lastName}} </td>
				<td> {{tempCustomer.email}} </td>
				<td ng-click="vm.update($index)"><a>Update</a></td>
				<td ng-click="vm.confirmDelete(tempCustomer.id)"><a>Delete</a></td>
			</tr>
		</table>
	
	</div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-resource.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/app.module.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/customer.controller.js"></script>
</body>
</html>