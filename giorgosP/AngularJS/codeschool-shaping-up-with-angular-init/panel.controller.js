/**
 * Created by gio on 25/10/2016.
 */
app.controller('PanelController',function(){
    var vm = this;

    vm.tab = 2;

    vm.selectTab = function(selectTab){
        vm.tab = selectTab;
    };

    vm.isSelected = function(checkTab){
        return vm.tab === checkTab;
    }
});
