/**
 * Service that shows communication between 2 controllers
 */
app.factory('sharedService',function(){

    var list = [];

    function getList(){
        return list;
    }
    function addElement(num){
        list.push(num);
    }

    return {
        getList : getList,
        addElement: addElement
    };


});