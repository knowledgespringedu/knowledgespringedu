/**
 * 
 */
app.factory('gemsService',['$http', function($http){
	
	function getGems(){
		var gemsArrWithReviews = [
	        {
	            name: 'Sapphire',
	            price: 2,
	            descr: 'It is a red gem',
	            canPurchase: true,
	            soldOut: false,
	            img: 'images/gem-05.gif',
	            reviews : [
	                {
	                    stars: 5,
	                    body: 'I love this product',
	                    author: 'john@gmail.com'
	                },
	                {
	                    stars: 1,
	                    body: 'this product sucks',
	                    author: 'lesley@gmail.com'
	                },
	                {
	                    stars: 3,
	                    body: 'Its ok',
	                    author: 'gogo@gmail.com'
	                }
	            ]
	        },
	        {
	            name: 'Saphire',
	            price: 3,
	            descr: 'It is a green gem',
	            canPurchase: true,
	            soldOut: false,
	            img: 'images/gem-02.gif',
	            reviews : [
	                {
	                    stars: 5,
	                    body: 'I love this product',
	                    author: 'john@gmail.com'
	                },
	                {
	                    stars: 1,
	                    body: 'this product sucks',
	                    author: 'lesley@gmail.com'
	                },
	                {
	                    stars: 3,
	                    body: 'Its ok',
	                    author: 'gogo@gmail.com'
	                }
	            ]
	        },
	        {
	            name: 'diamond',
	            price: 4.35,
	            descr: 'It is a blue gem',
	            canPurchase: true,
	            soldOut: false,
	            img: 'images/gem-03.gif',
	            reviews : [
	                {
	                    stars: 5,
	                    body: 'I love this product',
	                    author: 'john@gmail.com'
	                },
	                {
	                    stars: 1,
	                    body: 'this product sucks',
	                    author: 'lesley@gmail.com'
	                },
	                {
	                    stars: 3,
	                    body: 'Its ok',
	                    author: 'gogo@gmail.com'
	                }
	            ]
	        }
	    ];
		
		return gemsArrWithReviews;
	}
	
	function printGems(x){
		console.log('print '+x);
	}
	
	function getAPIGems(){
        return $http({
            method: 'GET',
            url: 'store-products.json'
        });
	}

	function deleteAPIGems(id){
        return $http({
            method: 'DELETE',
            url: 'http://localhost:8080/gems-angular/gem/deleteJSON/',
            params: {id: id}
        });
	}
	
	
	return {
		getGems : getGems,
		printGems: printGems,
		getAPIGems: getAPIGems,
        deleteAPIGems: deleteAPIGems
	};
	
	
}]);