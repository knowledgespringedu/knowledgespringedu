/**
 * Created by gio on 25/10/2016.
 */
app.controller('ReviewController',function(){
    var vm = this;
	
    vm.review = {};

    vm.addReview = function(x) {
        x.reviews.push(vm.review);
        vm.review = {};
    };
});