/**
 *
 * Service used for all HTTP requests
 *
 */
app.factory('httpService', ['$http', function($http) {
        
       
        function getJSON(url){
            if(!url) return;
            return $http({method:'GET', url: url});
        }
        function postJSON(url, params, data) {
            if(!url) return;
            return $http({method:'POST', url: url, params: params, data: data});
        }
        function putJSON(url, params, data) {
        	if(!url) return;
            return $http({method:'PUT', url: url, params: params, data: data});
        }
        function deleteJSON(url, params) {
            if(!url) return;
            return $http({method:'DELETE', url: url, params: params});
        }
        
        return {
            getJSON: getJSON,
            postJSON: postJSON,
            putJSON: putJSON,
            deleteJSON: deleteJSON
        };

    
}]);

