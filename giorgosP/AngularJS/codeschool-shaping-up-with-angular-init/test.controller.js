/**
 * Created by Giorgos Pap on 27/10/2016.
 */
app.controller('TestController',['sharedService',function(sharedService){
    var vm = this;

    //communication between 2 controllers through sharedService
    vm.inputNum = 0;
    vm.addElement = function(){
        sharedService.addElement(vm.inputNum);
    };

}]);