/**
 * Service to manipulate the gems and the reviews (GET/POST/PUT/DELETE) 
 */
app.factory('gemsService',['REST','httpService', function(REST, httpService){
	
	var url = REST.url;
	
	/**
	 * Gets all gems
	 */
	function getGems(){
	    return httpService.getJSON(url +'/gems');
	}
	/**
	 * Gets a gem by id
	 */
	function getGem(id){
		return httpService.getJSON(url +'/gems/'+id);
	}
	/**
	 * Creates a new gem
	 */
	function addGem(params, data){
		 return httpService.postJSON(url +'/gems', params, data);
	}
	/**
	 * Gets all reviews
	 */
	function getReviews(){
		return httpService.getJSON(url +'/review');
	}
	/**
	 * Gets a review by id
	 */
	function getReview(id){
		return httpService.getJSON(url +'/reviews/'+id);
	}
	/**
	 * Create a new review for a specific gem
	 */
	function addReview(id, params, data){
		return httpService.postJSON(url +'/gems/'+id+'review', params, data);
	}
	/**
	 * Update a review
	 */	
	function updateReview(params, data){
		return httpService.putJSON(url +'/reviews', params, data);
	}
	/**
	 * Delete a review
	 */
	function deleteReview(params){
		return httpService.deleteJSON(url +'/reviews', params);
	}
	
	
	return {
		getGems : getGems,
		getGem: getGem,
		addGem: addGem,
		getReviews: getReviews,
		getReview: getReview,
		addReview: addReview,
		updateReview: updateReview,
		deleteReview: deleteReview
	};
	
	
}]);