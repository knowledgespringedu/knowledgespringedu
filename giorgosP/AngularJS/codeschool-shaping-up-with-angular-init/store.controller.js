/**
 * Created by gio on 25/10/2016.
 */
app.controller('StoreController', ['gemsService', 'sharedService', function(gemsService, sharedService){

	var vm = this;
	
	vm.productsArrWithReviews = [];

	//communication between 2 controllers through sharedService
	vm.myList = [];
	vm.myList = sharedService.getList();

    // -- Get all Gems (static)
    //var gemsArrWithReviews = gemsService.getGems();

    // -- Get all Gems (dynamic through API)
   /* gemsService.getAPIGems()
        .then(function(results){
            vm.productsArrWithReviews = results.data;
        })
        .catch(function(err){
            console.log(err);
        });*/
    

    //-- Delete Gem by id
	/*gemsService.deleteAPIGems(1)
	 .then(function(results){
		 console.log(results);
		})
		.catch(function(err){
			console.log(err);
		});*/



	//--CONTROLLER SCOPE --

    //---1st method (recommended)
    //vm.product = gem;
    //vm.productsArr = gemsArr;
    //vm.productsArrWithReviews = gemsArrWithReviews;

    //---2nd method (avoid)
    //$scope.product = gem;
	
	/*gemsService.getGems()
	 .then(function(results){
        console.log(results);
     })
     .catch(function(err){
        console.log(err);
     });

	
	gemsService.getGem(2)
	 .then(function(results){
        console.log(results);
     })
     .catch(function(err){
        console.log(err);
     });
	
	 var gemParams = {
		gem_name: 'Diamond',
		gem_price: 2000,
		gem_descr: 'descr',
		gem_canPurchase: true
	 };
	
	 gemsService.addGem(gemParams,{})
	  .then(function(results){
         console.log(results);
      })
      .catch(function(err){
         console.log(err);
      });
	 
	  var reviewParams = {
		id: 2,
		review_stars: 5,
		review_body: 'It is a comment',
		review_author: 'george@gmail.com'
	  };
	
		
	  gemsService.addReview(2, reviewParams,{})
	   .then(function(results){
		   console.log(results);
       })
       .catch(function(err){
    	   console.log(err);
       });
	  
	   var updatedReviewParams = {
			id: 2,
			review_stars: 3,
			review_body: 'It is an updated comment',
			review_author: 'george@gmail.com'
	   };
			
				
	  gemsService.updateReview(updatedReviewParams,{})
	   .then(function(results){
		   console.log(results);
       })
       .catch(function(err){
    	   console.log(err);
       });
	  
	  
	  var deleteReviewParams ={
		id: 2  
	  };
	  
	  gemsService.deleteReview(deleteReviewParams,{})
	   .then(function(results){
		   console.log(results);
	   	})
	   	.catch(function(err){
	   	   console.log(err);
	    });*/

	  
	  
}]);

