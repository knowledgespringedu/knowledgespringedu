package com.practice.exercises;

public interface Vehicle {
	public String getType();
	public String getUpgradeProgram();
}
