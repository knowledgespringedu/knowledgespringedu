package com.practice.exercises;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Car implements Vehicle {
	@Autowired
	@Qualifier("engineUpgradeService")
	private UpgradeService service;

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "this is a car";
	}

	@Override
	public String getUpgradeProgram() {
		// TODO Auto-generated method stub
		return service.getProgram();
	}

}
