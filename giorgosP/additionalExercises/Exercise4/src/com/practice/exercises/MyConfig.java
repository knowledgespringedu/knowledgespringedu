package com.practice.exercises;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("com.practice.exercises")
public class MyConfig {
	
	@Bean(name="vehicle")
	public Vehicle car(){
		Car car = new Car();
		return car;
	}
	
	@Bean(name="motorcycle")
	public Vehicle motorcycle(){
		Motorcycle moto = new Motorcycle();
		return moto;
	}
}
