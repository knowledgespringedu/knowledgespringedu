package com.practice.exercises;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

	public static void main(String[] args) {

		//read spring config file
		AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(MyConfig.class);
		
		//Car vehicle = context.getBean("car", Car.class);
		Motorcycle vehicle = context.getBean("motorcycle", Motorcycle.class);
		
		System.out.println(vehicle.getType());
		System.out.println(vehicle.getUpgradeProgram());
		
		//close context
		context.close();
	}

}
