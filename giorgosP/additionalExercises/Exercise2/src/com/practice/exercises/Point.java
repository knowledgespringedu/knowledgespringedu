package com.practice.exercises;

import org.springframework.stereotype.Component;

@Component
public class Point {
	
	private int x;
	private int y;
	
	public Point(){
		//System.out.println("Inside Point constructor.");
	}

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
