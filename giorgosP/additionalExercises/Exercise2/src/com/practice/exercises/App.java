package com.practice.exercises;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

	public static void main(String[] args) {

		//read spring config file
		AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(MyConfig.class);
		
		Line myLine = context.getBean("myLine", Line.class);
		
		Point p1 = context.getBean("myPoint", Point.class);
		p1.setX(5);
		p1.setY(10);
		myLine.setPointA(p1);
		
		Point p2 = context.getBean("myPoint", Point.class);
		p2.setX(30);
		p2.setY(15);
		myLine.setPointB(p2);
		
		myLine.print();
		
		
		//close context
		context.close();
	}

}
