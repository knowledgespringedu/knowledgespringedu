package com.practice.exercises;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("com.practice.exercises")
public class MyConfig {
	
	@Bean(name="myLine")
	public Line line(){
		Line l = new Line();
		return l;
	}
	
	@Bean(name="myPoint")
	@Scope("prototype") // <-remove this line and run the code again
	public Point point(){
		Point p = new Point();
		return p;
	}
}
