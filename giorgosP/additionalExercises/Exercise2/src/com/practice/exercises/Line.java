package com.practice.exercises;

import org.springframework.stereotype.Component;

@Component
public class Line {
	private Point pointA;
	private Point pointB;
	
	public Line(){
		//System.out.println("Inside Line constructor.");
	}

	public Point getPointA() {
		return pointA;
	}

	public void setPointA(Point pointA) {
	   this.pointA = pointA;
	}

	public Point getPointB() {
        return pointB;
	}

	public void setPointB(Point pointB) {
        this.pointB = pointB;
	}

	public void print() {
		if(pointA == null || pointB == null){
			System.out.println("points not init");
		}
		System.out.println( "Line: A("+pointA.getX()+","+pointA.getY()+") -->" + " B("+pointB.getX()+","+pointB.getY()+")");
		
	}
}
