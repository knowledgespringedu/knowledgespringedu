package com.practice.exercises;

public class Line {
	private Point pointA;
	private Point pointB;
	
	public Line(){
		System.out.println("Inside Line constructor.");
	}

	public Point getPointA() {
		return pointA;
	}

	public void setPointA(Point pointA) {
	   this.pointA = pointA;
	}

	public Point getPointB() {
        return pointB;
	}

	public void setPointB(Point pointB) {
        this.pointB = pointB;
	}
}
