package com.practice.exercises;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {

		//read spring config file
		ClassPathXmlApplicationContext context=
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Line myLine = context.getBean("myLine", Line.class);
		
		//close context
		context.close();
	}

}
