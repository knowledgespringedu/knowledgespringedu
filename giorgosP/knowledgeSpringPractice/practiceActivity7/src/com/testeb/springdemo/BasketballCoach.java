package com.testeb.springdemo;

import org.springframework.beans.factory.annotation.Autowired;

public class BasketballCoach implements Coach {
	
	@Autowired
	private FortuneService fortuneService;
	
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "exercise your 3point shots";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}

}
