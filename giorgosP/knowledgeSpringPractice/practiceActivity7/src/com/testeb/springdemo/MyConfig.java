package com.testeb.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ComponentScan("com.testeb.springdemo") <--- ignore this
public class MyConfig {
	
	@Bean
	public FortuneService newFortuneService(){
		NewFortuneService newFortuneService = new NewFortuneService();
		return newFortuneService;
	}
	
	@Bean
	public Coach basketballCoach(){
		BasketballCoach basketballCoach = new BasketballCoach();
		return basketballCoach;
	}
}
