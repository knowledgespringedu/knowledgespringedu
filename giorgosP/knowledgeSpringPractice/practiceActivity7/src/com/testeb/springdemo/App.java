package com.testeb.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {

		//read spring config file
		/*ClassPathXmlApplicationContext context=
				new ClassPathXmlApplicationContext("applicationContext.xml");*/
		
		AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(MyConfig.class);
		
		//get the bean from spring container	
		Coach theCoach=context.getBean("basketballCoach",Coach.class);
		
		//call a method on the bean
		System.out.println(theCoach.getDailyWorkout());
		
		//call method to get daily fortune
		System.out.println(theCoach.getDailyFortune());
		
		//close context
		context.close();
	}

}
