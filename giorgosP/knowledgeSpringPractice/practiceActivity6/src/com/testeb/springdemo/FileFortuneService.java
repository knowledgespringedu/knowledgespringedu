package com.testeb.springdemo;

import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileFortuneService implements FortuneService {
	
	@Value("#{'${fortunes}'.split(',')}")
	private List<String> fortunesList;
	
	public FileFortuneService(){
		//empty constructor
	}
	
	@PostConstruct
	public void domyInit(){
		System.out.println("@PostConstruct executed");
	}
	
	
	@Override
	public String getFortune() {
		//return random element
		return "Random fortune is: "+fortunesList.get(new Random().nextInt(fortunesList.size()));
	}
	
	

}
