package giorgos.test2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MyConfig {
	
	@Bean(name="line")
	public Line line(){
		Line line = new Line();
		return line;
	}
	
	@Bean(name="pointA1")
	@Scope("prototype") // <-remove this line and run the code again by g_pap
	public Point point(){
		Point p = new Point();
		return p;
	}
	
	
}
