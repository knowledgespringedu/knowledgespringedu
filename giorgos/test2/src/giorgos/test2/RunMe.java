package giorgos.test2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RunMe {

	public static void main(String[] args) {
		//get the beans
		//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(MyConfig.class);
		
		Line myLine = context.getBean("line", Line.class);
		
		Point p1 = context.getBean("pointA1", Point.class);
		Point p2 = context.getBean("pointA1", Point.class);
		
		//System.out.println("line p1 = " + p1.getX() + " , " + p1.getY());
		//System.out.println("line p2 = " + p2.getX() + " , " + p2.getY());
		
		p1.setX(5);
		p1.setY(10);
		myLine.setPointA(p1);
		
		p2.setX(11.2);
		p2.setY(10);
		myLine.setPointB(p2);
		
		//myLine.setPointA(p1);
		//myLine.setPointB(p2);
		
		System.out.println("line setted at points p1.x = " + myLine.getPointA().getX() + " and p1.y = " + myLine.getPointA().getY());
		System.out.println("line setted at points p2.x = " + myLine.getPointB().getX() + " and p2.y = " + myLine.getPointB().getY());
		//close context
		context.close();
	}

}
