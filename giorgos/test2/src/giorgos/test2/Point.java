package giorgos.test2;

public class Point {
	//declare point values (x,y) 
	private double x;
	private double y;
	
	public Point() {
		System.out.println("PointCreated");
	}
	
	//Get Point values
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	//Set Point Values
	public double setX(double x){
		this.x = x;
		return x;
	}
	
	public double setY(double y){
		this.y = y;
		return y;
	}

}
