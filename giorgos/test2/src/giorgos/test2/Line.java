package giorgos.test2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

public class Line {

	// Line has 2 points
	@Autowired
	private Point pointA;
	
	@Autowired
	private Point pointB;
	
	public Line() {
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * set and get points
	 */
	public Point getPointA() {
        return pointA;
	}

	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}

	public Point getPointB() {
        return pointB;
	}

	public void setPointB(Point pointB) {
        this.pointB = pointB;
	}
}
