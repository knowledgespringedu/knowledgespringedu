package giorgos.test3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class StraightLine implements Line {

	@Override
	public String getLineType() {
		// TODO Auto-generated method stub
		return "it is a straigt line ";
	}
	
	
	//+++++++++++++++++++++++++++++
	@Autowired
	@Qualifier("fixStyleService")
	private ChangeLineService service;
	
	public String getChangeLineProp() {
		// TODO Auto-generated method stub
		return service.changeLineProp();
	}

}
