package giorgos.test3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class RunMe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(MyConfig.class);
		
		CurlyLine crline = context.getBean("curlyLine", CurlyLine.class);
		StraightLine stline = context.getBean("straightLine", StraightLine.class);
		
		System.out.println(crline.getLineType());
		System.out.println(stline.getLineType());
		
		System.out.println(crline.getChangeLineProp());
		System.out.println(stline.getChangeLineProp());
		
		context.close();
	}

}
