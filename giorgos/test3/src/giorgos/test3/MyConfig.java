package giorgos.test3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("giorgos.test3")
public class MyConfig {
	
	@Bean(name="straightLine")
	public Line straightLine(){
		StraightLine straightLine = new StraightLine();
		return straightLine;
	}
	
	@Bean(name="curlyLine")
	public Line curlyLine(){
		CurlyLine curlyLine = new CurlyLine();
		return curlyLine;
	}
	
	
}
