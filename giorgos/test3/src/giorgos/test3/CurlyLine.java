package giorgos.test3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CurlyLine implements Line {

	@Override
	public String getLineType() {
		// TODO Auto-generated method stub
		return "it is a curly line ";
	}
	
	
	//+++++++++++++++++++++++++++++
	@Autowired
	@Qualifier("fixColourService")
	private ChangeLineService service;
	
	public String getChangeLineProp() {
		// TODO Auto-generated method stub
		return service.changeLineProp();
	}
	

}
