package com.testeb.springdemo.dao;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerDAO {

	public List<Customer> getCustomers();
	public Customer getPersonById(int id);
	public void updatePerson(Customer person);
	public void deletePerson(int id);
	public void addCustomer(Customer c);
}
