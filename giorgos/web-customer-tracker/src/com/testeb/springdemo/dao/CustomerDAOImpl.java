package com.testeb.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	//need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;//ειναι ίδιο με το id του, στο xml
	
	@Override
	@Transactional
	public List<Customer> getCustomers() {

		//get the current hibernate session
		Session currentSession=sessionFactory.getCurrentSession();
		
		//create a query
		Query<Customer> theQuery =currentSession.createQuery("from Customer",Customer.class);
		
		//execute query and get result list
		List<Customer> customers=theQuery.getResultList();
		
		//return the results
		return customers;
	}
	
/*	//�����������
	public void updCustomer(Integer custId ){
		
		System.out.println("epejergasia");
	}
	
	//��������
	public void delCustomer(Integer custId ){
		
		System.out.println("diagrafi");
	}
	
	//��������
	public void insCustomer(){
	
		System.out.println("eisagwgh");
	}*/
	
	@Override
	@Transactional
	public Customer getPersonById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Customer c= session.get(Customer.class,id);
		return c;
	}
	
	
	@Override
	@Transactional
	public void updatePerson(Customer person) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(person);
	}

	@Override
	@Transactional
	public void deletePerson(int personid) {
		Session session = this.sessionFactory.getCurrentSession();
		Customer person = session.get(Customer.class,personid);
		
		System.out.println(person);
		
		session.delete(person);
	}
	
	@Override
	@Transactional
	public void addCustomer(Customer c){
		Session session = sessionFactory.getCurrentSession();
		session.persist(c);
	}
	
}
