package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import com.testeb.springdemo.dao.CustomerDAO;
import com.testeb.springdemo.entity.Customer;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	//need to inject the DAO 
	@Autowired
	private CustomerDAO customerDAO;
	
	@RequestMapping("/list")
	public String showList(Model theModel){
		//get customers from DAO
		List<Customer> theCustomers = customerDAO.getCustomers();
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		
		Customer person = new Customer();
		theModel.addAttribute("person",person);
		
		return "list-customer";
	}
	
	
	@GetMapping("/updPeople/{id}")
	public String updPerson(@PathVariable("id") int id,Model model){
		Customer person= customerDAO.getPersonById(id);
		model.addAttribute("person",person);
		
		return "updPeople";
	}
	
	
	@RequestMapping(value = "/update")
		public String updateCustomer(@ModelAttribute("person") Customer person){
		System.out.println(person);
		customerDAO.updatePerson(person);
		return "redirect:/customer/list";
	}
	
	@RequestMapping("/delPeople/{id}")
    public String removeCustomer( @PathVariable("id") int id){
		customerDAO.deletePerson(id);
        return "redirect:/customer/list";
    }
	
	@GetMapping("/insPerson")
	public String insPerson(Model model){
		Customer person = new Customer();
		model.addAttribute("person",person);
		
		return "insPerson";
	}
	
	
	@RequestMapping(value= "/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("person") Customer person){
		customerDAO.addCustomer(person);
		return "redirect:/customer/list";
		
	}
	
	
}
