<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert Person</title>
</head>
<body>
<h2>Upd Person</h2>
<form:form action="add" method="post" modelAttribute="person">
	<table>
          <tr>
              <form:label path="firstName">
               <td>Name:</td>
               </form:label>
              <td><form:input path="firstName" /></td>
           </tr>
          <tr>
            <form:label path="lastName">
            <td>Last Name:</td>
            </form:label>
            <td><form:input path="lastName" /></td>
           </tr>
          <tr>
          	<form:label path="email">
              <td>Email:</td>
               </form:label>
              <td><form:input path="email" /></td>
		</tr>
		<tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"></td>
    	</tr>
	</table>
</form:form>
<p>
<a href="${pageContext.request.contextPath}">go back</a>
</body>
</html>