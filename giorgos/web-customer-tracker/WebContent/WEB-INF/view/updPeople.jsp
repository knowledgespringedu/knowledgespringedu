<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Upd Person</title>
</head>
<body>
<h2>Upd Person</h2>
<form:form action="../update" modelAttribute="person" method="post">
id: <form:input path="id"/>
<br>
First Name: <form:input path="firstName"/>
<br>
Last Name: <form:input path="lastName"/>
<br>
Email: <form:input path="email"/>
<br>
<input type="submit" value="Update" />
</form:form>
<p>
<a href="../list">go back</a>
</body>
</html>