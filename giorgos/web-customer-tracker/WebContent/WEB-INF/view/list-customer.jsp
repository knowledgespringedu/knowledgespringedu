<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>

<head>
	<title>  List Customers</title>
	<link type="text/css"
		rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/style.css">
		
</head>
<body>
	<div id="wrapper">
		<div id="header">
		
			<h2>CRM - Customer Relationship Manager</h2>
		</div>
		
	</div>
	<div id="container">
		
		<table>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th> 
				<th>Upd</th>
				<th>delete</th>
			</tr>
			
			<!--  loop over and print our customers -->
			<c:forEach var="tempCustomer" items="${customers}">
				<tr>
					<td> ${tempCustomer.firstName} </td>
					<td> ${tempCustomer.lastName} </td>
					<td> ${tempCustomer.email} </td>
					
					<td> <a href="updPeople/${tempCustomer.id}" />Upd</a></td>
					
					<td> <a href="<c:url value='delPeople/${tempCustomer.id}' />" >delete</a></td>
				</tr>
			</c:forEach>
		
		</table>
		<br>
		<a href="insPerson">insert</a>
	<form:form action="add" method="post" modelAttribute="person">
	<table>
          <tr>
              <form:label path="firstName">
               <td>Name:</td>
               </form:label>
              <td><form:input path="firstName" /></td>
           </tr>
          <tr>
            <form:label path="lastName">
            <td>Last Name:</td>
            </form:label>
            <td><form:input path="lastName" /></td>
           </tr>
          <tr>
          	<form:label path="email">
              <td>Email:</td>
               </form:label>
              <td><form:input path="email" /></td>
		</tr>
		<tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"></td>
    	</tr>
	</table>
</form:form>
	
	</div>
</body>
</html>