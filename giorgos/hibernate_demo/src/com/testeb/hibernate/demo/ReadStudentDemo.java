package com.testeb.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.testeb.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {

		//create session factory
		SessionFactory factory=new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		//create a session
		Session session=factory.getCurrentSession();
		
		
		//
		try{
			
			
			
			//create a studen object
			System.out.println("Creating new student object...");
			Student tempStudent= new Student("Daffy","Duck","duffy@knowledge.gr");
			
			//start a transaction
			session.beginTransaction();
			
			//save the sudent object
			System.out.println("Saving the student...");
			System.out.println(tempStudent);
			session.save(tempStudent);
			
			//commit transaction
			session.getTransaction().commit();
			
			//MyNewCode
			
			//find out the student's id:primary key
			System.out.println("Saved student. Generated id: "+tempStudent.getId());;

			//now get a new session and start transaction
			session =factory.getCurrentSession();
			session.beginTransaction();
			//retrieve student based on the id: primary key
			System.out.println("\nGetting student with id: "+tempStudent.getId());
			
			Student myStudent= session.get(Student.class, tempStudent.getId());
			System.out.println("GET complete: "+ myStudent);

			//commit the transaction
			session.getTransaction().commit();
			
			
			
			System.out.println("DONE!!");

			
		}
		finally{
			factory.close();
		}
		
		
		
	}

}
