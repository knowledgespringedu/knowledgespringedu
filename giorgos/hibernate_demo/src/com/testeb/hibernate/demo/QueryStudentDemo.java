package com.testeb.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.testeb.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {

		//create session factory
		SessionFactory factory=new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		//create a session
		Session session=factory.getCurrentSession();
		
		
		//
		try{
			
			
			//start a transaction
			session.beginTransaction();
			
			//query student
			List<Student> theStudents= session.createQuery("from Student").getResultList();
			
			
			//display students
			displayStudents(theStudents);
			
			//query student :lastName="Testempasis"
			theStudents=session.createQuery("from Student s where s.lastName='Testempasis'").getResultList();
			
			//display students with last name Testempasis
			displayStudents(theStudents);
			
			
			//query student : lastName="Testempasis" OR firstName="Thanos1"
			theStudents=session.createQuery("from Student s where s.lastName='Testempasis' OR s.firstName='Thanos1'").getResultList();
			
			//display students  : lastName="Testempasis" OR firstName="Thanos1"
			displayStudents(theStudents);			
			
			
			//query student : lastName="Testempasis" OR firstName="Thanos1"
			theStudents=session.createQuery("from Student s where s.email LIKE '%knowledge.gr'").getResultList();
			
			//display students  : lastName="Testempasis" OR firstName="Thanos1"
			displayStudents(theStudents);			
			
			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done!!");
		}
		finally{
			factory.close();
		}
		
		
		
	}

	private static void displayStudents(List<Student> theStudents) {
		for (Student tempStudent : theStudents){
			System.out.println(tempStudent);
		}
	}

}
