package giorgos.test1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import giorgos.test1.Coach;

public class runMe {

	public static void main(String[] args) {
		//read spring config file
		//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		
		//use config.java
		AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(Config.class);
		

		//Reference the new coach implementation in your main application.
		Coach theCoach=context.getBean("coach",Coach.class);
		System.out.println(theCoach.getDailyWorkout());
		
		//autowire fortune
		System.out.println(theCoach.getFortune());
		
		context.close();
	}

}
