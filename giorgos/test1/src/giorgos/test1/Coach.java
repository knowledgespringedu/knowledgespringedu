package giorgos.test1;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import giorgos.test1.FortuneService;

@Component
public class Coach {
	//Define a new Coach implementation using Annotations
	
	@Autowired
	private FortuneService fortuneService;
	
	@PostConstruct
	public void FortuneInit(){
		System.out.println("postConstruct");
		System.out.println( fortuneService.getFortune());
	}
	
	@PreDestroy
	public void FortuneDestroy(){
		System.out.println("preDestroy");
		System.out.println(fortuneService.getFortune());
	}
	
	public String getFortune() {
		return fortuneService.getFortune();
	}
	
	public String getDailyWorkout() {
		return "Daily Run at 6.00!";
	}
}
