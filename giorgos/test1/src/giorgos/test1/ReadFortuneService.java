package giorgos.test1;

import java.util.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ReadFortuneService implements FortuneService {
	public ReadFortuneService() {
		// TODO Auto-generated constructor stub
	}

	//getValues Fortune
	@Value("${foo.ft1}")
	private String ft1;
	
	@Value("${foo.ft2}")
	private String ft2;
	
	@Value("${foo.ft3}")
	private String ft3;
	
	ArrayList<String> list = new ArrayList<String>();
		
	@Override
	public String getFortune() {
		list.add(ft1);
		list.add(ft2);
		list.add(ft3);
		return list.get(new Random().nextInt(list.size()));
	}

}
