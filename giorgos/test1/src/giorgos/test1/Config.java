package giorgos.test1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:fortunes.properties")
public class Config {
	public Config() {	}

	@Bean
	public FortuneService readFortuneService(){
		ReadFortuneService readFortuneService = new ReadFortuneService();
		return readFortuneService;
	}
	
	@Bean
	public Coach coach(){
		Coach coach = new Coach();
		return coach;
	}
}
