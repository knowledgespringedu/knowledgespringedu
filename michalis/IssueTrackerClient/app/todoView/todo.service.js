/* global todoApp */

todoApp.factory('noteService', function ($http) {
    function getNotes(type) {
	return $http.get('http://localhost:8084/pjRest/todo/notes', {
	    params: {
		type: type
	    }
	});
    }

    function createNote(body) {
	return $http.post('http://localhost:8084/pjRest/todo/notes', null, {
	    params: {
		text: body
	    }
	});
    }

    function updateNote(note) {
	return $http.put('http://localhost:8084/pjRest/todo/notes', null, {
	    params: {
		id: note.id,
		text: note.body,
		checked: note.checked
	    }
	});
    }

    function deleteNote(note) {
	return $http.delete('http://localhost:8084/pjRest/todo/notes', {
	    params: {
		id: note.id
	    }
	});
    }

    return {
	getNotes: getNotes,
	createNote: createNote,
	updateNote: updateNote,
	deleteNote: deleteNote
    };
});