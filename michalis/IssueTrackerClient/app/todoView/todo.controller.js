/* global todoApp */

todoApp.controller('NotesController', function (noteService) {
    var ctrl = this;
    ctrl.type = 'open';	// Holds currently selected tab
    ctrl.noteList = [];

    ctrl.getNotes = function (type) {
	noteService.getNotes(type).then(function (result) {
	    ctrl.noteList = result.data;
	    ctrl.type = type;	// Sets currently selected tab according to data fetched
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.getNotes(ctrl.type);	// Initialize data

    ctrl.createNote = function (body) {
	noteService.createNote(body).then(function (result) {
	    ctrl.getNotes(ctrl.type);	// Fetch current tab's data
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.updateNote = function (note) {
	note.checked = true;
	noteService.updateNote(note).then(function (result) {
	    ctrl.getNotes(ctrl.type);	// Fetch current tab's data
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.deleteNote = function (note) {
	noteService.deleteNote(note).then(function (result) {
	    ctrl.getNotes(ctrl.type);	// Fetch current tab's data
	}).catch(function (err) {
	    console.log(err);
	});
    };
});