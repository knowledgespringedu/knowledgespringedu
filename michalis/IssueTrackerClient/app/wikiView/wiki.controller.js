/* global wikiApp */

wikiApp.controller('WikiController', function (wikiService, $sce) {
    var ctrl = this;
    ctrl.wiki;	// Shows currently selected wiki, null if "New" wiki is selected
    ctrl.wikiList = [];
    ctrl.showEditor = true; // Flag controlling editor|preview views

    ctrl.getWikis = function () {
	wikiService.getWikis().then(function (result) {
	    ctrl.wikiList = result.data;
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.getWikis();	// Populate form on load

    ctrl.createWiki = function (wiki) {
	wikiService.createWiki(wiki).then(function (result) {
	    ctrl.wikiList = result.data;
	    ctrl.wiki = null;	// Empty form after creation
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.updateWiki = function (wiki) {
	wikiService.updateWiki(wiki).then(function (result) {
	    ctrl.wikiList = result.data;
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.deleteWiki = function (wiki) {
	wikiService.deleteWiki(wiki).then(function (result) {
	    if (ctrl.wiki && wiki.id === ctrl.wiki.id) {
		ctrl.wiki = null;   // Reset to "New button" selected
	    }
	    ctrl.wikiList = result.data;
	}).catch(function (err) {
	    console.log(err);
	});
    };

    /**
     * Function handling upsert calls of wikis.
     *
     * @param {wiki} wiki If it contains id then wiki is updated, else inserted
     * @returns {null}
     */
    ctrl.handleForm = function (wiki) {
	if ('id' in wiki) {
	    ctrl.updateWiki(wiki);
	} else {
	    ctrl.createWiki(wiki);
	}
    };

    /**
     * Function required by @function ng-bind-html to sanitize input html.
     *
     * @param {wiki} wiki Wiki object of the element calling function
     * @returns {String} Secure string or empty string
     */
    ctrl.getTrustedBody = function (wiki) {
	if (wiki) {
	    return $sce.trustAsHtml(wiki.body);
	} else {
	    return '';
	}
    };

    /**
     * Returns active state of en element depending on currently selected wiki.
     * Depends on @link ctrl.wiki.
     *
     * @param {wiki} wiki Wiki object of the element calling function
     * @returns {String} Class 'active' or empty string
     */
    ctrl.checkActive = function (wiki) {
	if ((!ctrl.wiki || !ctrl.wiki.id) && !wiki) {
	    return 'active';	// Make "New" button active
	}
	return ctrl.wiki === wiki ? 'active' : '';  // Only occurs on listed wikis, not "New" button
    };
});

