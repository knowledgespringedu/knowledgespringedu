/* global wikiApp */

wikiApp.factory('wikiService', function ($http) {
    function getWikis() {
	return $http.get('http://localhost:8084/pjRest/wiki');
    }

    function createWiki(wiki) {
	return $http.post('http://localhost:8084/pjRest/wiki', null, {
	    params: {
		title: wiki.title,
		description: wiki.description,
		text: wiki.body
	    }
	});
    }

    function updateWiki(wiki) {
	return $http.put('http://localhost:8084/pjRest/wiki', null, {
	    params: {
		id: wiki.id,
		title: wiki.title,
		description: wiki.description,
		text: wiki.body
	    }
	});
    }

    function deleteWiki(wiki) {
	return $http.delete('http://localhost:8084/pjRest/wiki', {
	    params: {
		id: wiki.id
	    }
	});
    }

    return {
	getWikis: getWikis,
	createWiki: createWiki,
	updateWiki: updateWiki,
	deleteWiki: deleteWiki
    };
});

