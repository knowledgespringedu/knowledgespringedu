/* global issuesApp */

issuesApp.controller('IssuesController', function (issuesService) {
    var ctrl = this;
    ctrl.type = 'unresolved';	// Holds currently selected tab
    ctrl.isFormShown = false;	// Flag controlling form visibility
    ctrl.issue;			// Currently selected issue
    ctrl.issueList = [];

    ctrl.getIssues = function (type) {
	issuesService.getIssues(type).then(function (result) {
	    ctrl.issueList = result.data;
	    ctrl.type = type;	// Store data's filter of tab
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.getIssues(ctrl.type);

    ctrl.createIssue = function (issue) {
	issuesService.createIssue(issue).then(function (result) {
	    ctrl.getIssues(ctrl.type);	// Reload current tab's data
	    ctrl.isFormShown = false;
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.updateIssue = function () {
	issuesService.updateIssue(ctrl.issue).then(function (result) {
	    ctrl.getIssues(ctrl.type);	// Reload current tab's data
	    ctrl.isFormShown = false;
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.resolveIssue = function (issue) {
	issue.resolved = true;
	issuesService.updateIssue(issue).then(function (result) {
	    ctrl.getIssues(ctrl.type);	// Reload current tab's data
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.deleteIssue = function (issue) {
	issuesService.deleteIssue(issue).then(function (result) {
	    ctrl.getIssues(ctrl.type);	// Reload current tab's data
	}).catch(function (err) {
	    console.log(err);
	});
    };

    /**
     * Decide list element's background based on
     * issue's @property {String} priority.
     *
     * @param {issue} issue Element's associated issue object
     * @returns {String} CSS background color class
     */
    ctrl.getPriorityColor = function (issue) {
	if (issue.resolved) {
	    return 'list-group-item-success';
	}

	switch (issue.priority) {
	    case 'low':
		return 'list-group-item-info';
	    case 'high':
		return 'list-group-item-danger';
	    case 'normal':
	    default:
		return '';
	}
    };

    /**
     * Decides whether to show issue upsert form.
     *
     * @param {issue} issue Element's associated issue object
     */
    ctrl.showForm = function (issue) {
	ctrl.isFormShown = true;
	ctrl.issue = issue;
    };

    /**
     * Handles upsert decisions, calling the appropriate method depending on
     * whether exists issue's id.
     *
     * @param {issue} issue The issue to be upserted
     */
    ctrl.handleForm = function (issue) {
	if ('id' in issue) {
	    ctrl.updateIssue(issue);
	} else {
	    ctrl.createIssue(issue);
	}
    };
});