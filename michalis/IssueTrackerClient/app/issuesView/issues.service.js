/* global issuesApp */

issuesApp.factory('issuesService', function ($http) {
    function getIssues(type) {
	return $http.get('http://localhost:8084/pjRest/issues', {
	    params: {
		type: type
	    }
	});
    }

    function createIssue(issue) {
	return $http.post('http://localhost:8084/pjRest/issues', null, {
	    params: {
		title: issue.title,
		text: issue.text,
		priority: issue.priority,
		assignedTo: issue.assignee
	    }
	});
    }

    function updateIssue(issue) {
	return $http.put('http://localhost:8084/pjRest/issues', null, {
	    params: {
		id: issue.id,
		title: issue.title,
		text: issue.text,
		priority: issue.priority,
		assignedTo: issue.assignee,
		resolved: issue.resolved
	    }
	});
    }

    function deleteIssue(issue) {
	return $http.delete('http://localhost:8084/pjRest/issues', {
	    params: {
		id: issue.id
	    }
	});
    }

    return {
	getIssues: getIssues,
	createIssue: createIssue,
	updateIssue: updateIssue,
	deleteIssue: deleteIssue
    };
});