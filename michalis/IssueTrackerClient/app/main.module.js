var app = angular.module('mainApp', ['angularWidget', 'ngSanitize'])
	.config(function initializemanifestGenerator(widgetsProvider) {
	    widgetsProvider.setManifestGenerator(function () {
		return function (name) {
		    return {
			module: name + 'App',
			html: name + 'View/' + name + '.html',
			files: [
			    name + 'View/' + name + '.controller.js',
			    name + 'View/' + name + '.service.js'
			]
		    };
		};
	    });
	});