package com.knowledge.issuetracker.dao;

import com.knowledge.issuetracker.entity.WikiEntity;
import java.util.List;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
public interface WikiDao {

    public List<WikiEntity> getWikis();

    public void createWiki(WikiEntity wiki);

    public void updateWiki(WikiEntity wiki);

    public void deleteWiki(WikiEntity wiki);

}
