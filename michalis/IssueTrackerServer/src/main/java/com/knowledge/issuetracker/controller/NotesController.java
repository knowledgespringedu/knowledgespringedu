package com.knowledge.issuetracker.controller;

import com.knowledge.issuetracker.entity.ToDoEntity;
import com.knowledge.issuetracker.service.NotesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
@RestController
@CrossOrigin
@RequestMapping("/pjRest/todo/notes")
public class NotesController {

    @Autowired
    NotesService notesService;

    @GetMapping
    @ResponseBody
    public List<ToDoEntity> getNotes(@RequestParam(name = "type", defaultValue = "") String checked) {
	switch (checked) {
	    case "checked":
		return notesService.getCheckedNotes();
	    case "open":
		return notesService.getOpenNotes();
	    default:
		return notesService.getNotes();
	}
    }

    @PostMapping
    @ResponseBody
    public List<ToDoEntity> createNote(@RequestParam("text") String text) {
	ToDoEntity note = new ToDoEntity();
	note.setBody(text);
	notesService.createNote(note);

	return notesService.getNotes();
    }

    @PutMapping
    @ResponseBody
    public List<ToDoEntity> updateNote(@RequestParam("id") long id,
	    @RequestParam("text") String text,
	    @RequestParam("checked") boolean checked) {
	ToDoEntity note = new ToDoEntity();
	note.setId(id);
	note.setBody(text);
	note.setChecked(checked);
	notesService.updateNote(note);

	return notesService.getNotes();
    }

    @DeleteMapping
    @ResponseBody
    public List<ToDoEntity> deleteNote(@RequestParam("id") long id) {
	ToDoEntity note = new ToDoEntity();
	note.setId(id);
	notesService.deleteNote(note);

	return notesService.getNotes();
    }
}
