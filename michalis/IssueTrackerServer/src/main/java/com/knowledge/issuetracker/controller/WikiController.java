package com.knowledge.issuetracker.controller;

import com.knowledge.issuetracker.entity.WikiEntity;
import com.knowledge.issuetracker.service.WikiService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
@RestController
@CrossOrigin
@RequestMapping("/pjRest/wiki")
public class WikiController {

    @Autowired
    WikiService wikiService;

    @GetMapping
    @ResponseBody
    public List<WikiEntity> getWikis() {
	return wikiService.getWikis();
    }

    @PostMapping
    @ResponseBody
    public List<WikiEntity> createWiki(@RequestParam("title") String title,
	    @RequestParam("description") String description,
	    @RequestParam("text") String text) {
	WikiEntity wiki = new WikiEntity();
	wiki.setTitle(title);
	wiki.setBody(text);
	wiki.setDescription(description);
	wikiService.createWiki(wiki);

	return wikiService.getWikis();
    }

    @PutMapping
    @ResponseBody
    public List<WikiEntity> updateNote(@RequestParam("id") long id,
	    @RequestParam("title") String title,
	    @RequestParam("description") String description,
	    @RequestParam("text") String text) {
	WikiEntity wiki = new WikiEntity();
	wiki.setId(id);
	wiki.setTitle(title);
	wiki.setDescription(description);
	wiki.setBody(text);
	wikiService.updateWiki(wiki);

	return wikiService.getWikis();
    }

    @DeleteMapping
    @ResponseBody
    public List<WikiEntity> deleteNote(@RequestParam("id") long id) {
	WikiEntity wiki = new WikiEntity();
	wiki.setId(id);
	wikiService.deleteWiki(wiki);

	return wikiService.getWikis();
    }

}
