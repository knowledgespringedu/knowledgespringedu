package com.knowledge.issuetracker.dao;

import com.knowledge.issuetracker.entity.IssueEntity;
import java.util.List;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
public interface IssuesDao {

    public List<IssueEntity> getIssues(boolean resolved);

    public void createIssue(IssueEntity issue);

    public void updateIssue(IssueEntity issue);

    public void deleteIssue(IssueEntity issue);

}
