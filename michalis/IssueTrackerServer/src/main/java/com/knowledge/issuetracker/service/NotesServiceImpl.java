package com.knowledge.issuetracker.service;

import com.knowledge.issuetracker.dao.NotesDao;
import com.knowledge.issuetracker.entity.ToDoEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NotesServiceImpl implements NotesService {

    @Autowired
    private NotesDao notesDao;

    @Override
    @Transactional
    public List<ToDoEntity> getNotes() {
	return notesDao.getNotes();
    }

    @Override
    @Transactional
    public List<ToDoEntity> getCheckedNotes() {
	return notesDao.getCheckedNotes();
    }

    @Override
    @Transactional
    public List<ToDoEntity> getOpenNotes() {
	return notesDao.getOpenNotes();
    }

    @Override
    @Transactional
    public void createNote(ToDoEntity note) {
	notesDao.createNote(note);
    }

    @Override
    @Transactional
    public void updateNote(ToDoEntity note) {
	notesDao.updateNote(note);
    }

    @Override
    @Transactional
    public void deleteNote(ToDoEntity note) {
	notesDao.deleteNote(note);
    }

}
