package com.knowledge.issuetracker.dao;

import com.knowledge.issuetracker.entity.WikiEntity;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WikiDaoImpl implements WikiDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<WikiEntity> getWikis() {
	return sessionFactory.getCurrentSession().createQuery("from WikiEntity").getResultList();
    }

    @Override
    public void createWiki(WikiEntity wiki) {
	if (wiki != null) {
	    sessionFactory.getCurrentSession().save(wiki);
	}
    }

    @Override
    public void updateWiki(WikiEntity wiki) {
	if (wiki != null && wiki.getId() > 0) {
	    sessionFactory.getCurrentSession().update(wiki);
	}
    }

    @Override
    public void deleteWiki(WikiEntity wiki) {
	if (wiki != null && wiki.getId() > 0) {
	    sessionFactory.getCurrentSession().delete(wiki);
	}
    }
}
