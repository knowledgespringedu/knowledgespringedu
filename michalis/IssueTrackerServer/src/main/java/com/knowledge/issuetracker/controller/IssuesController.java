package com.knowledge.issuetracker.controller;

import com.knowledge.issuetracker.entity.IssueEntity;
import com.knowledge.issuetracker.service.IssuesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
@RestController
@CrossOrigin
@RequestMapping("/pjRest/issues")
public class IssuesController {

    @Autowired
    IssuesService issuesService;

    @GetMapping
    @ResponseBody
    public List<IssueEntity> getIssues(@RequestParam(name = "type", defaultValue = "unresolved") String resolved) {
	switch (resolved) {
	    case "resolved":
		return issuesService.getResolvedIssues();
	    case "open":
	    default:
		return issuesService.getUnresolvedIssues();
	}
    }

    @PostMapping
    @ResponseBody
    public List<IssueEntity> createIssue(@RequestParam("title") String title,
	    @RequestParam("text") String text,
	    @RequestParam("priority") String priority,
	    @RequestParam("assignedTo") String assignee) {
	IssueEntity issue = new IssueEntity();
	issue.setTitle(title);
	issue.setText(text);
	issue.setPriority(priority);
	issue.setAssignee(assignee);
	issuesService.createIssue(issue);

	return issuesService.getUnresolvedIssues();
    }

    @PutMapping
    @ResponseBody
    public List<IssueEntity> updateIssue(@RequestParam("id") long id,
	    @RequestParam(name = "resolved", required = false) boolean resolved,
	    @RequestParam(name = "text", required = false) String text,
	    @RequestParam(name = "title", required = false) String title,
	    @RequestParam(name = "priority", required = false) String priority,
	    @RequestParam(name = "assignedTo", required = false) String assignee) {
	IssueEntity issue = new IssueEntity();
	issue.setId(id);
	issue.setResolved(resolved);
	issue.setTitle(title);
	issue.setPriority(priority);
	issue.setText(text);
	issue.setAssignee(assignee);
	issuesService.updateIssue(issue);

	if (issue.getResolved()) {
	    return issuesService.getResolvedIssues();
	} else {
	    return issuesService.getUnresolvedIssues();
	}
    }

    @DeleteMapping
    @ResponseBody
    public List<IssueEntity> deleteIssue(@RequestParam("id") long id) {
	IssueEntity issue = new IssueEntity();
	issue.setId(id);
	issuesService.deleteIssue(issue);

	if (issue.getResolved()) {
	    return issuesService.getResolvedIssues();
	} else {
	    return issuesService.getUnresolvedIssues();
	}
    }
}
