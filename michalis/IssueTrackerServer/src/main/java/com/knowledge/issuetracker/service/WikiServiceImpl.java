package com.knowledge.issuetracker.service;

import com.knowledge.issuetracker.dao.WikiDao;
import com.knowledge.issuetracker.entity.WikiEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WikiServiceImpl implements WikiService {

    @Autowired
    WikiDao wikiDao;

    @Override
    @Transactional
    public List<WikiEntity> getWikis() {
	return wikiDao.getWikis();
    }

    @Override
    @Transactional
    public void createWiki(WikiEntity wiki) {
	wikiDao.createWiki(wiki);
    }

    @Override
    @Transactional
    public void updateWiki(WikiEntity wiki) {
	wikiDao.updateWiki(wiki);
    }

    @Override
    @Transactional
    public void deleteWiki(WikiEntity wiki) {
	wikiDao.deleteWiki(wiki);
    }

}
