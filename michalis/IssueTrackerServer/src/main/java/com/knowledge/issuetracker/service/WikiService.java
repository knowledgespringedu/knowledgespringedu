package com.knowledge.issuetracker.service;

import com.knowledge.issuetracker.entity.WikiEntity;
import java.util.List;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
public interface WikiService {

    public List<WikiEntity> getWikis();

    public void createWiki(WikiEntity wiki);

    public void updateWiki(WikiEntity wiki);

    public void deleteWiki(WikiEntity wiki);

}
