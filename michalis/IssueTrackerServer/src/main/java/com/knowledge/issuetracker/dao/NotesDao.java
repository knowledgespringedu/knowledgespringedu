package com.knowledge.issuetracker.dao;

import com.knowledge.issuetracker.entity.ToDoEntity;
import java.util.List;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
public interface NotesDao {

    public List<ToDoEntity> getNotes();

    public List<ToDoEntity> getCheckedNotes();

    public List<ToDoEntity> getOpenNotes();

    public void createNote(ToDoEntity note);

    public void updateNote(ToDoEntity note);

    public void deleteNote(ToDoEntity note);
}
