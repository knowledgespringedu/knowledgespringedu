package com.knowledge.issuetracker.service;

import com.knowledge.issuetracker.entity.IssueEntity;
import java.util.List;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
public interface IssuesService {

    public List<IssueEntity> getUnresolvedIssues();

    public List<IssueEntity> getResolvedIssues();

    public void createIssue(IssueEntity note);

    public void updateIssue(IssueEntity note);

    public void deleteIssue(IssueEntity note);

}
