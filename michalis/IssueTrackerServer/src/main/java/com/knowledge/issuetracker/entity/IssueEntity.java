package com.knowledge.issuetracker.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Mike Drakoulelis <drakouleli@ceid.upatras.gr>
 */
@Entity
@Table(name = "TB_ISSUES")
public class IssueEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "issue_id")
    private Long id;

    @Column(name = "issue_title")
    private String title;

    @Column(name = "issue_body")
    private String text;

    @Column(name = "resolved")
    private Boolean resolved = false;

    @Enumerated(EnumType.STRING)
    @Column(name = "issues_prio")
    private Priority priority = Priority.normal;

    @Column(name = "issue_assignTo")
    private String assignee;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public Boolean getResolved() {
	return resolved;
    }

    public void setResolved(Boolean resolved) {
	this.resolved = resolved;
    }

    public Priority getPriority() {
	return priority;
    }

    public void setPriority(Priority priority) {
	this.priority = priority;
    }

    public void setPriority(String priority) {
	switch (priority) {
	    case "normal":
		setPriority(Priority.normal);
		return;
	    case "low":
		setPriority(Priority.low);
		return;
	    case "high":
		setPriority(Priority.high);
	}
    }

    public String getAssignee() {
	return assignee;
    }

    public void setAssignee(String assignee) {
	this.assignee = assignee;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof IssueEntity)) {
	    return false;
	}
	IssueEntity other = (IssueEntity) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.knowledge.issuetracker.entity.IssueEntity[ id=" + id + " ]";
    }

    private enum Priority {
	normal,
	low,
	high
    }

}
