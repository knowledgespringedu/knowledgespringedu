package com.knowledge.issuetracker.dao;

import com.knowledge.issuetracker.entity.IssueEntity;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IssuesDaoImpl implements IssuesDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<IssueEntity> getIssues(boolean resolved) {
	return sessionFactory.getCurrentSession().createQuery("from IssueEntity where resolved = " + resolved).getResultList();
    }

    @Override
    public void createIssue(IssueEntity issue) {
	if (issue != null) {
	    sessionFactory.getCurrentSession().save(issue);
	}
    }

    @Override
    public void updateIssue(IssueEntity issue) {
	if (issue != null && issue.getId() > 0) {
	    sessionFactory.getCurrentSession().update(issue);
	}
    }

    @Override
    public void deleteIssue(IssueEntity issue) {
	if (issue != null && issue.getId() > 0) {
	    sessionFactory.getCurrentSession().delete(issue);
	}
    }

}
