package com.knowledge.issuetracker.dao;

import com.knowledge.issuetracker.entity.ToDoEntity;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NotesDaoImpl implements NotesDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<ToDoEntity> getNotes() {
	return sessionFactory.getCurrentSession().createQuery("from ToDoEntity", ToDoEntity.class).getResultList();
    }

    @Override
    public List<ToDoEntity> getCheckedNotes() {
	return sessionFactory.getCurrentSession().createQuery("from ToDoEntity where checked=true", ToDoEntity.class).getResultList();
    }

    @Override
    public List<ToDoEntity> getOpenNotes() {
	return sessionFactory.getCurrentSession().createQuery("from ToDoEntity where checked=false", ToDoEntity.class).getResultList();
    }

    @Override
    public void createNote(ToDoEntity note) {
	if (note != null) {
	    sessionFactory.getCurrentSession().save(note);
	}
    }

    @Override
    public void updateNote(ToDoEntity note) {
	if (note != null && note.getId() > 0) {
	    sessionFactory.getCurrentSession().update(note);
	}
    }

    @Override
    public void deleteNote(ToDoEntity note) {
	if (note != null && note.getId() > 0) {
	    sessionFactory.getCurrentSession().delete(note);
	}
    }
}
