package com.knowledge.issuetracker.service;

import com.knowledge.issuetracker.dao.IssuesDao;
import com.knowledge.issuetracker.entity.IssueEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IssuesServiceImpl implements IssuesService {

    @Autowired
    IssuesDao issuesDao;

    @Override
    @Transactional
    public List<IssueEntity> getUnresolvedIssues() {
	return issuesDao.getIssues(false);
    }

    @Override
    @Transactional
    public List<IssueEntity> getResolvedIssues() {
	return issuesDao.getIssues(true);
    }

    @Override
    @Transactional
    public void createIssue(IssueEntity issue) {
	issuesDao.createIssue(issue);
    }

    @Override
    @Transactional
    public void updateIssue(IssueEntity issue) {
	issuesDao.updateIssue(issue);
    }

    @Override
    @Transactional
    public void deleteIssue(IssueEntity issue) {
	issuesDao.deleteIssue(issue);
    }

}
