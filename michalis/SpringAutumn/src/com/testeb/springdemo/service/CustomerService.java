package com.testeb.springdemo.service;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerService {

    public List<Customer> getCustomers();

    public int createCustomer(Customer newCustomer);

    public void deleteCustomer(int customerId);

    public void updateCustomer(Customer customer);
}
