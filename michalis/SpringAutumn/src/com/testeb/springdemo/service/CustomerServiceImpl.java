package com.testeb.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.dao.CustomerDAO;
import com.testeb.springdemo.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

    //inject customer dao
    @Autowired
    private CustomerDAO customerDAO;

    @Override
    @Transactional
    public List<Customer> getCustomers() {

	return customerDAO.getCustomers();
    }

    @Override
    @Transactional
    public int createCustomer(Customer newCustomer) {
	return customerDAO.createCustomer(newCustomer);
    }

    @Override
    @Transactional
    public void deleteCustomer(int customerId) {
	customerDAO.deleteCustomer(customerId);
    }

    @Override
    @Transactional
    public void updateCustomer(Customer customer) {
	customerDAO.updateCustomer(customer);
    }

}
