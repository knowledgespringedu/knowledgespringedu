package com.testeb.springdemo.dao;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerDAO {

    public List<Customer> getCustomers();

    public int createCustomer(Customer newCustomer);

    public void deleteCustomer(int id);

    public void updateCustomer(Customer customer);
}
