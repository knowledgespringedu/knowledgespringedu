package com.testeb.springdemo.dao;

import com.testeb.springdemo.entity.Customer;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Customer> getCustomers() {
	Session currentSession = sessionFactory.getCurrentSession();
	return currentSession.createQuery("from Customer", Customer.class).getResultList();
    }

    @Override
    public int createCustomer(Customer newCustomer) {
	Session currentSession = sessionFactory.getCurrentSession();
	return (int) currentSession.save(newCustomer);
    }

    @Override
    public void deleteCustomer(int id) {
	for (Customer customer : getCustomers()) {
	    if (customer.getId() == id) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.delete(customer);
		break;
	    }
	}
    }

    @Override
    public void updateCustomer(Customer customer) {
	Session currentSession = sessionFactory.getCurrentSession();
	currentSession.update(customer);
    }

}
