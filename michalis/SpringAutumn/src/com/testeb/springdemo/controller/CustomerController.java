package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.testeb.springdemo.entity.Customer;
import com.testeb.springdemo.service.CustomerService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public String showList(Model theModel) {

	//get customers from DAO
	List<Customer> theCustomers = customerService.getCustomers();

	//add the customers to the model
	theModel.addAttribute("customers", theCustomers);
	theModel.addAttribute("customer", new Customer());

	return "list-customer";
    }

    @PostMapping
    public String insertCustomer(@ModelAttribute("customer") Customer newCustomer) {
	customerService.createCustomer(newCustomer);
	return "redirect:/";
    }

    @DeleteMapping("/{id}")
    public String deleteCustomer(@PathVariable int id) {
	customerService.deleteCustomer(id);
	return "redirect:/";
    }

    @PostMapping("/update")
    public String updateCustomer(@ModelAttribute("customer") Customer customer) {
	customerService.updateCustomer(customer);
	return "redirect:/";
    }
}
