<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>
    <head>
	<title>  List Customers</title>
	<link type="text/css"
	      rel="stylesheet"
	      href="${pageContext.request.contextPath}/resources/css/style.css">
	<script src="/resources/js/jquery/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
	<div id="wrapper">
	    <div id="header">

		<h2>CRM - Customer Relationship Manager</h2>
	    </div>

	</div>
	<div id="container">
	    <table>
		<tr>
		    <th>First Name</th>
		    <th>Last Name</th>
		    <th>Email</th>
		</tr>

		<!--  loop over and print our customers -->
		<c:forEach var="tempCustomer" items="${customers}">
		    <tr data-id="${tempCustomer.id}">
			<td data-field="name"> ${tempCustomer.firstName} </td>
			<td data-field="surname"> ${tempCustomer.lastName} </td>
			<td data-field="email"> ${tempCustomer.email} </td>
			<td data-field="edit"><button onClick="editCustomer(this)">Edit</button></td>
			<td><button onClick="deleteCustomer(${tempCustomer.id})">Delete</button></td>
		    </tr>
		</c:forEach>
	    </table>

	    <script>
		function deleteCustomer(id) {
		    $.ajax({
			url: '/customer/' + id,
			type: 'DELETE'
		    }).always(function () {
			location.reload(true);
		    });
		}

		function editCustomer(button) {
		    var tr = $(button).closest('tr');
		    $('#id').val(tr.data('id'));
		    $('#firstName').val(tr.children('[data-field="name"]').html());
		    $('#lastName').val(tr.children('[data-field="surname"]').html());
		    $('#email').val(tr.children('[data-field="email"]').html());
		    $('#updateForm').show();
		}
	    </script>

	    <form method="POST">
		<input type="text" name="firstName" value="John" />
		<input type="text" name="lastName" value="Doe" />
		<input type="email" name="email" value="johndoe@knowledge.gr" />
		<input type="submit" value="Insert" />
	    </form>

	    <form:form method="put" action="/customer/update" modelAttribute="customer" id="updateForm" cssStyle="display: none;">
		<form:hidden path="id" id="id"/>
		<form:input path="firstName" id="firstName"/>
		<form:input path="lastName" id="lastName"/>
		<form:input path="email" id="email"/>
		<input type="submit" value="Update" />
	    </form:form>
	</div>
    </body>
</html>