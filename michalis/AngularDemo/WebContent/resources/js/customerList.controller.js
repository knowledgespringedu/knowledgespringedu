/* global app */

app.controller('CustomerListController', function (customerService) {

    var ctrl = this;
    ctrl.customers = [];

    customerService.getCustomers().then(function (result) {
	ctrl.customers = result.data;
    }).catch(function (err) {
	console.log(err);
    });

    ctrl.deleteCustomer = function (id) {
	customerService.deleteCustomer(id).then(function (result) {
	    ctrl.customers = result.data;
	}).catch(function (err) {
	    console.log(err);
	});
    };

    ctrl.editCustomer = function (customer) {
	return customer;
    };
});