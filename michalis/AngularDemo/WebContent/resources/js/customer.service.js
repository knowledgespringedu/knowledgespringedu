/* global app */

app.factory('customerService', function ($http) {

    function getCustomers() {
	return $http.get('http://localhost:8084/customerRestService/customer');
    }

    function getCustomer(id) {
	return $http.get('http://localhost:8084/customerRestService/customer/' + id);
    }

    function deleteCustomer(id) {
	return $http({
	    method: 'DELETE',
	    url: 'http://localhost:8084/customerRestService/customer',
	    params: {
		id: id
	    }
	});
    }

    function addCustomer(params) {
	return $http({
	    method: 'POST',
	    url: 'http://localhost:8084/customerRestService/customer',
	    params: params
	});
    }

    function updateCustomer(params) {
	return $http({
	    method: 'PUT',
	    url: 'http://localhost:8084/customerRestService/customer',
	    params: params
	});
    }

    return {
	getCustomers: getCustomers,
	deleteCustomer: deleteCustomer,
	addCustomer: addCustomer,
	updateCustomer: updateCustomer,
	getCustomer: getCustomer
    };
});