/* global app */

app.controller('UpdateCustomerController', function (customerService, $location, $routeParams) {
    var ctrl = this;
    ctrl.customer = {};

    ctrl.customer = customerService.getCustomer($routeParams.customerId).then(function (result) {
	ctrl.customer = result.data;
    }).catch(function (err) {
	console.log(err);
    });

    ctrl.updateCustomer = function () {
	customerService.updateCustomer(ctrl.customer).then(function (result) {
	    $location.path('');
	    ctrl.customer = {};
	}).catch(function (err) {
	    console.log(err);
	});
    };
});