/* global $route */

var app = angular.module('app', ['ngRoute'])
	.config(function ($routeProvider) {
	    $routeProvider.when('/showFormForUpdate/:customerId', {
		templateUrl: '/resources/templates/updateForm.html',
		controller: 'UpdateCustomerController as itemDetailsCtrl',
		resolve: {
		    /*async: ['customerService', '$route', function (customerService, $route) {
		     return customerService.getCustomer($route.current.params.id);
		     }]*/
		}
	    }).when('/', {
		templateUrl: '/resources/templates/customerList.html'
	    }).when('/showFormForAdd', {
		templateUrl: '/resources/templates/addForm.html'
	    }).otherwise({
		redirectTo: '/'
	    });
	});