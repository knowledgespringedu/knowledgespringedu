/* global app */

app.controller('NewCustomerController', function (customerService, $location) {

    var ctrl = this;
    ctrl.customer = {};

    ctrl.addCustomer = function () {
	customerService.addCustomer(ctrl.customer).then(function (result) {
	    $location.path('');
	    ctrl.customer = {};
	}).catch(function (err) {
	    console.log(err);
	});
    };
});