package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.testeb.springdemo.entity.Customer;
import com.testeb.springdemo.service.CustomerService;

@RestController
@RequestMapping("/customerRestService")
public class CustomerRestController {

    @Autowired
    private CustomerService customerService;

    //Get all Customer
    @GetMapping(value = "/customer")
    public @ResponseBody
    List<Customer> getCustomers() {

	//get customers from DAO
	List<Customer> theCustomers = customerService.getCustomers();
	System.out.println("teste");
	return theCustomers;
    }

    @GetMapping(value = "/customer/{id}")
    @ResponseBody
    public Customer getCustomerWithId(@PathVariable("id") int id) {
	return customerService.getCustomerWithId(id);
    }

    //Delete method with required id parameter
    @DeleteMapping(value = "/customer")
    @ResponseBody
    public List<Customer> getCustomersAfterdelete(@RequestParam("id") int id) {

	customerService.deleteCustomer(id);
	List<Customer> theCustomers = customerService.getCustomers();
	System.out.println("delete Service closed");

	return theCustomers;
    }

    //Post create Method with no id (auto -generated from db)
    @PostMapping(value = "/customer")
    @ResponseBody
    public List<Customer> getCustomersAfterPost(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("email") String email) {

	Customer newCustomer = new Customer();
	newCustomer.setFirstName(firstName);
	newCustomer.setLastName(lastName);
	newCustomer.setEmail(email);
	customerService.addCustomer(newCustomer);
	List<Customer> theCustomers = customerService.getCustomers();
	System.out.println("post service closed");

	return theCustomers;
    }

    //Put update Method with all required parameters
    @PutMapping(value = "/customer")
    @ResponseBody
    public List<Customer> updateCustomer(@RequestParam("id") int id, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("email") String email) {

	Customer newCustomer = customerService.getCustomerWithId(id);
	newCustomer.setFirstName(firstName);
	newCustomer.setLastName(lastName);
	newCustomer.setEmail(email);

	customerService.updateCustomer(newCustomer);
	List<Customer> theCustomers = customerService.getCustomers();
	System.out.println("put(update) service closed");
	return theCustomers;
    }

}
