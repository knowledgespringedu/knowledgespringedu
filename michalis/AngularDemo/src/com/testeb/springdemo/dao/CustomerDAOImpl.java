package com.testeb.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

    //need to inject the session factory
    @Autowired
    private SessionFactory sessionFactory;//ειναι ίδιο με το id του, στο xml

    @Override
    //@Transactional    note it because we put it on service layer
    public List<Customer> getCustomers() {

	//get the current hibernate session
	Session currentSession = sessionFactory.getCurrentSession();

	//create a query
	Query<Customer> theQuery = currentSession.createQuery("from Customer order by lastName", Customer.class);

	//execute query and get result list
	List<Customer> customers = theQuery.getResultList();

	//return the results
	return customers;
    }

    @Override
    public void deleteCustomer(int id) {

	//get the current hibernate session
	Session currentSession = sessionFactory.getCurrentSession();
	Customer customer = (Customer) currentSession.load(Customer.class, new Integer(id));
	if (null != customer) {
	    currentSession.delete(customer);
	}
	System.out.println("Dept deleted successfully, Dept Details =" + customer);

    }

    @Override
    public void addCustomer(Customer c) {

	Session currentSession = sessionFactory.getCurrentSession();

	if (null != c) {
	    currentSession.save(c);
	    System.out.println("Customer added successfully, Customer Details =" + c);

	} else {

	    System.out.println("Null Customer!!!");
	}
    }

    @Override
    public void updateCustomer(Customer c) {
	Session currentSession = sessionFactory.getCurrentSession();

	if ((null != c) && c.getId() != 0) {
	    currentSession.update(c);
	    System.out.println("Customer updated successfully, Customer Details =" + c);

	} else {

	    System.out.println("Null Customer!!!");
	}
    }

    @Override
    public Customer getCustomer(int id) {
	Session currentSession = sessionFactory.getCurrentSession();
	Customer customer = currentSession.get(Customer.class, id);
	if (null != customer) {
	    System.out.println("Customer  get By id, Customer Details =" + customer);
	} else {
	    System.out.println("Customer could not found =" + customer);

	}
	return customer;

    }

}
