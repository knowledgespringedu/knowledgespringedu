package com.testeb.springdemo.service;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerService {

    public List<Customer> getCustomers();

    public void deleteCustomer(int id);

    public void addCustomer(Customer c);

    public void updateCustomer(Customer c);

    public Customer getCustomerWithId(int id);

}
