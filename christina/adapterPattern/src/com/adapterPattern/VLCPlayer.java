package com.adapterPattern;

public class VLCPlayer implements AdvancedMediaPlayer{

	@Override 
	public void playVLC (String filename){
		System.out.println("Playing playvlc" + filename);
	}
	
	@Override 
	public void playMP4 (String filename){
		
	}
	
}
