package com.adapterPattern;

public interface AdvancedMediaPlayer {
	
	public void playVLC (String filename);
	
	public void playMP4(String filename);

}
