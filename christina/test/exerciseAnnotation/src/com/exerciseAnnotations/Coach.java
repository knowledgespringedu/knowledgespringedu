package com.exerciseAnnotations;

public interface Coach {

	public String getDailyWorkout();
	public String getDailyFortune();
	
}
