package com.exerciseAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("basketballPlayer")
@Scope("prototype")
public class BasketballCoach implements Coach {

	
	@Value("$foo.email")
	private String email;
	
	@Autowired
	@Qualifier("chriFortune")
	private FortuneService fortuneService;
	
	public BasketballCoach(){
		
	}
	
	@PostConstruct
	public void domyInit(){
		
		
	}
	
	@PreDestroy
	public void domyDestroy(){
		System.out.println(email);
		
	}
	
	@Override
	public String getDailyWorkout() {
		return "Line 1: Basketball Player-class getDailyWorkout-method";
	}

	@Override
	public String getDailyFortune() {
		return "Line 2: Basketball-class" + fortuneService.getFortune() + " method";
	}
}
