package com.exerciseAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainPresentCoach {

	public static void main(String[] args){
		
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//AnnotationConfigApplicationContext context=  new AnnotationConfigApplicationContext(ConfigClass.class);
		
		Coach tennisCoach = context.getBean("idOfTheTennisCoach",Coach.class);

		System.out.println(tennisCoach.getDailyWorkout());

		System.out.println(tennisCoach.getDailyFortune());
		
		Coach volleyballCoach = context.getBean("basketballPlayer",Coach.class);
		
		System.out.println(volleyballCoach.getDailyWorkout());

		System.out.println(volleyballCoach.getDailyFortune());

		
		context.close();
		
	}
	
}
