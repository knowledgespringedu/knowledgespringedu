package com.exerciseAnnotations;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestCoach {
	
	@Test
	public void testCreateCoach()throws Exception{
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//get the bean from spring container
		
		Coach theCoach=context.getBean("idOfTheTennisCoach",Coach.class);
		//call a method on the bean
		
		System.out.println(theCoach.getDailyWorkout());
		
		context.close();
		
	}

}
