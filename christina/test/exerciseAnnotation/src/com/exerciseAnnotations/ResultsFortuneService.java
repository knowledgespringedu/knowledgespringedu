package com.exerciseAnnotations;

import org.springframework.stereotype.Component;


@Component("chriFortune")
public class ResultsFortuneService implements FortuneService{

	@Override
	public String getFortune() {
		return "Results for the daily fortune";
	}
	
}
