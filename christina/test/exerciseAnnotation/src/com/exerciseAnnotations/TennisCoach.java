package com.exerciseAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component("idOfTheTennisCoach")
@Scope("prototype")
public class TennisCoach implements Coach{

	@Value("$foo.email")
	private String email;
	
	@Autowired      //it works instead of the constructor
	@Qualifier ("chriFortune")    // searches for the implementations of FortuneService and returns back the one with this specific id 
	private FortuneService fortuneService;
	
	public TennisCoach(){
		
	}
	
	/*@Autowired     // this is not needed because of autowired qualifier
	public TennisCoach(ResultsFortuneService theFortuneService){
		fortuneService=theFortuneService;
	}
	
	/*@Autowired
	public void setFortuneService(FortuneService theFortuneService){
		System.out.println("TennisCoach class inside setter method fortuneservice");
		fortuneService=theFortuneService;
	}*/
	
	
	
	@Override
	public String getDailyWorkout() {
		return "Line 1: Tennis Coach-Class getDailyWorkout-method";
	}

	@Override
	public String getDailyFortune() {
		return "Line 2: " + fortuneService.getFortune() + " method";
	}
	
	@PostConstruct     // put it in the end
	public void domyInit(){
		
	}
	
	@PreDestroy     //put it in the end
	public void domyDestroy(){
		System.out.println(email);		
	}
}
