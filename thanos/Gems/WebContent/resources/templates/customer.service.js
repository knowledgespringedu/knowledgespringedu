/**
 * Created by thanostestebasis on 23/09/16.
 */
app.factory('Customer',['$http',function($http){
    //returns obj of resource class
    function getCustomers(){
        return $http.get('http://localhost:8080/web-customer-tracker/customerRestService/customer');

    }



    function removeCustomer(id) {
        return $http({method:'DELETE',url:'http://localhost:8080/web-customer-tracker/customerRestService/customer',params: {id:id}});


    }





    return {
    	getCustomers:getCustomers,
    	removeCustomer:removeCustomer
    };


    /*$http('http://localhost:8080/hello',
        {},//params
        {
            delete:$resource('http://localhost:8080/remove/:id',{id:'@id'})
        });

*/



}]);