<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>

<html>

<head>
	<title>  List Customers</title>
	<link type="text/css"
		rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/style.css">
		
</head>
<body>
	<div id="wrapper">
		<div id="header">
		
			<h2>CRM - Customer Relationship Manager</h2>
		</div>
		
	</div>
	
	<div id="container">
	<input type="button" value="Add Customer"
	onclick="window.location.href='showFormForAdd';return false"
	class="add-button"/>
	
	</div>
	
	
	<div id="container">
	
		<table>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Edit</th>
												<th>Delete</th>

			</tr>
			
			<!--  loop over and print our customers -->
			<c:forEach var="tempCustomer" items="${customers}">
				<tr>
					<td> ${tempCustomer.firstName} </td>
					<td> ${tempCustomer.lastName} </td>
					<td> ${tempCustomer.email} </td>
				  <td>
				  <a href="showFormForUpdate/${tempCustomer.id}">Edit</a>
				  </td>
                <td><a href="delete/${tempCustomer.id}">Delete</a>  </td> 
				</tr>
			</c:forEach>
		
		</table>
	
	
	
	</div>
</body>
</html>