package com.testeb.springdemo.service;

import java.util.List;

import com.testeb.springdemo.entity.Gem;

public interface GemService {

	public List<Gem> getGems();
	public void deleteGem(long id); 
	public void addGem(Gem c);
	public void updateGem(Gem c);
	public Gem getGem(long id);
}
