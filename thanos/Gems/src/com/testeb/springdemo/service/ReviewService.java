package com.testeb.springdemo.service;

import java.util.List;

import com.testeb.springdemo.entity.Review;

public interface ReviewService {
	public List<Review> getReviews();
	public void deleteReview(long id); 
	public void addReview(Review r);
	public void updateReview(Review r);
	public Review getReview(long id);
}
