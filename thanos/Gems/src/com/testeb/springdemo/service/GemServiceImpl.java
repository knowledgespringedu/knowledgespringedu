package com.testeb.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.dao.GemDAO;
import com.testeb.springdemo.entity.Gem;
import com.testeb.springdemo.entity.Review;

@Service
public class GemServiceImpl implements GemService {
	
	@Autowired
	private GemDAO gemDAO;
	
	
	
	@Override
	@Transactional
	public List<Gem> getGems() {
		// TODO Auto-generated method stub
		return gemDAO.getGems();
		}

	@Override
	@Transactional
	public void deleteGem(long id) {
		// TODO Auto-generated method stub
		this.gemDAO.deleteGem(id);

	}

	@Override
	@Transactional
	public void addGem(Gem g) {

		this.gemDAO.addGem(g);
	}

	@Override
	@Transactional
	public void updateGem(Gem g) {
		// TODO Auto-generated method stub

		this.gemDAO.updateGem(g);
	}

	@Override
	@Transactional
	public Gem getGem(long id) {
		// TODO Auto-generated method stub
		
		return this.gemDAO.getGem(id);
	}

}
