package com.testeb.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.dao.ReviewDAO;
import com.testeb.springdemo.entity.Review;

@Service
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	private ReviewDAO reviewDao;
	
	@Override
	@Transactional
	public List<Review> getReviews() {
		// TODO Auto-generated method stub
		return reviewDao.getReviews();
	}

	@Override
	@Transactional
	public void deleteReview(long id) {
		// TODO Auto-generated method stub
		this.reviewDao.deleteReview(id);

	}

	@Override
	@Transactional
	public void addReview(Review r) {
		// TODO Auto-generated method stub
		this.reviewDao.addReview(r);
	}

	@Override
	@Transactional
	public void updateReview(Review r) {
		// TODO Auto-generated method stub
		this.reviewDao.updateReview(r);

	}

	@Override
	@Transactional
	public Review getReview(long id) {
		// TODO Auto-generated method stub
		return this.reviewDao.getReview(id);
	}

}
