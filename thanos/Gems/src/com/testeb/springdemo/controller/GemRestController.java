package com.testeb.springdemo.controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.testeb.springdemo.entity.Review;
import com.testeb.springdemo.entity.Gem;
import com.testeb.springdemo.service.GemService;
import com.testeb.springdemo.service.ReviewService;

@RestController
@CrossOrigin
public class GemRestController {

	
	
	@Autowired
	private GemService gemService;
	
	@Autowired
	private ReviewService reviewService;
	
	//Get all Gems
	@GetMapping(value="/gems")
	@ResponseBody
	public  List<Gem> getGems(){
		 
		//get customers from DAO
		List<Gem> theGem = gemService.getGems();
		//Gem theGem=gemService.getGem(2);
		System.out.println(theGem);
		
		return theGem;
	}
	
	//Get gem by id
	@GetMapping(value="/gems/{id}")
	@ResponseBody
	public  Gem getGemById(@PathVariable("id")long id){
		 
		//get gem from DAO
		Gem theGem = gemService.getGem(id);
		//Gem theGem=gemService.getGem(2);
		//System.out.println(theGem);
		
		return theGem;
	}
	
	
	//add new Gem
	@PostMapping(value="/gems")
	@ResponseBody
	public  List<Gem> getCustomersAfterPost(@RequestParam("gem_name")String gem_name,@RequestParam("gem_price")double gem_price,@RequestParam("gem_descr")String gem_descr
			,@RequestParam("gem_canPurchase")Boolean gem_canPurchase){
		
		Gem newGem=new Gem();
		newGem.setGem_name(gem_name);
		newGem.setGem_price(gem_price);
		newGem.setGem_descr(gem_descr);
		newGem.setGem_canPurchase(gem_canPurchase);
		
		gemService.addGem(newGem);
		
		 List<Gem> theGems = gemService.getGems();
			System.out.println("post service closed");
			
			
		return theGems;
	}
	
	
	
	
	
	
	
	@PostMapping(value="/gems/{id}/review")
	@ResponseBody
	public  List<Gem> addReview(@PathVariable("id")long id,@RequestParam("review_stars")int review_stars,
			@RequestParam("review_body")String review_body,@RequestParam("review_author")String review_author){
		

		Gem gem = gemService.getGem(id);
		
		//Review emp1 = new Review(2, "testDescr", "test@gmail.com");
		Review review = new Review(review_stars, review_body,review_author);

		review.setGem(gem);

		reviewService.addReview(review);
		
		//reviewService.addReview(emp2);
		//gemService.updateGem(gem);
		
		return gemService.getGems();

		
	}
	
	@PutMapping(value="/review")
	@ResponseBody
	public  List<Gem> updateReview(@RequestParam("id")long id,@RequestParam("review_stars")int review_stars,
			@RequestParam("review_body")String review_body,@RequestParam("review_author")String review_author){
		

		Review review = reviewService.getReview(id);
		review.setReview_stars(review_stars);
		review.setReview_body(review_body);
		review.setReview_author(review_author);
		
		reviewService.updateReview(review);
		
		//reviewService.addReview(emp2);
		//gemService.updateGem(gem);
		
		return gemService.getGems();

		
	}
	
	//get all reviews
	@GetMapping(value="/review")
	@ResponseBody
	public  List<Review> getReviews(){
		
			 
			//get customers from DAO
			List<Review> reviews = reviewService.getReviews();
			System.out.println(reviews);
			
			return reviews;

		
	}
	
	//delete review
	@DeleteMapping(value="/review")
	@ResponseBody
	public List<Review> deleteReview(@RequestParam("id")long id){
		reviewService.deleteReview(id);
		
		List<Review> reviews = reviewService.getReviews();
		return reviews;
	}
	
	//delete review
		@DeleteMapping(value="/gems")
		@ResponseBody
		public List<Gem> deleteGem(@RequestParam("id")long id){
			gemService.deleteGem(id);
			
			List<Gem> gems = gemService.getGems();
			return gems;
		}
	
}
