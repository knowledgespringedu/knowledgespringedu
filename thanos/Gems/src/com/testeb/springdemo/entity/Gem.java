package com.testeb.springdemo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name="gem")
public class Gem {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="gem_id")
	private Long gem_id;
	
	@Column(name="gem_name")
	private String gem_name;
	
	@Column(name="gem_price")
	private Double gem_price;
	
	@Column(name="gem_descr")
	private String gem_descr;
	
	@Column(name="gem_canPurchase")
	private Boolean gem_canPurchase;
	
	@OneToMany(targetEntity=Review.class,mappedBy="gem"
			,cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	//@JsonBackReference
	@JsonManagedReference
	private List<Review> reviews;

	public Gem(){
		
	}
	public Long getGem_id() {
		return gem_id;
	}

	public void setGem_id(Long gem_id) {
		this.gem_id = gem_id;
	}

	public String getGem_name() {
		return gem_name;
	}

	public void setGem_name(String gem_name) {
		this.gem_name = gem_name;
	}

	public Double getGem_price() {
		return gem_price;
	}

	public void setGem_price(Double gem_price) {
		this.gem_price = gem_price;
	}

	public String getGem_descr() {
		return gem_descr;
	}

	public void setGem_descr(String gem_descr) {
		this.gem_descr = gem_descr;
	}

	public Boolean getGem_canPurchase() {
		return gem_canPurchase;
	}

	public void setGem_canPurchase(Boolean gem_canPurchase) {
		this.gem_canPurchase = gem_canPurchase;
	}

	// @JsonIgnore
	public List<Review> getReviews() {
		return reviews;
	}
	 //@JsonIgnore
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}
	

	
	 
	

	
}
