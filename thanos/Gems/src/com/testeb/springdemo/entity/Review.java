package com.testeb.springdemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.testeb.springdemo.entity.Gem;

@Entity
@Table(name="review")
public class Review {
	
	
	@Id
	@GeneratedValue
	@Column(name="review_id")
	private int review_id;
	
	@Column(name="review_stars")
	private int review_stars;
	
	@Column(name="review_body")
	private String review_body;
	
	@Column(name="review_author")
	private String review_author;
	
	@ManyToOne
	@JoinColumn(name="gem_id")
	//@JsonManagedReference
	@JsonBackReference
	private Gem gem;
	
	
	

	public Review() {
			
		}
	
	public Review(int review_stars, String review_body, String review_author) {
		this.review_stars = review_stars;
		this.review_body = review_body;
		this.review_author = review_author;
	}

	public int getReview_id() {
		return review_id;
	}

	public void setReview_id(int review_id) {
		this.review_id = review_id;
	}

	public int getReview_stars() {
		return review_stars;
	}

	public void setReview_stars(int review_stars) {
		this.review_stars = review_stars;
	}

	public String getReview_body() {
		return review_body;
	}

	public void setReview_body(String review_body) {
		this.review_body = review_body;
	}

	public String getReview_author() {
		return review_author;
	}

	public void setReview_author(String review_author) {
		this.review_author = review_author;
	}

	 //@JsonIgnore
	public Gem getGem() {
		return gem;
	}
	// @JsonIgnore
	public void setGem(Gem gem) {
		this.gem = gem;
	}

}
