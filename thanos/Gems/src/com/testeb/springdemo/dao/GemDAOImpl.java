package com.testeb.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.testeb.springdemo.entity.Gem;

@Repository
public class GemDAOImpl implements GemDAO {

	//need to inject the session factory
	@Autowired
	private SessionFactory sessionFactoryGem;
			
//		 sessionFactory = new AnnotationConfiguration()
//                .addAnnotatedClass(Gem.class)
//                .addAnnotatedClass(Review.class)
//                .configure()
//                .buildSessionFactory();
		
		
	@Override
	public List<Gem> getGems() {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		Query<Gem> theQuery =currentSession.createQuery("from Gem",Gem.class);
		//execute query and get result list
		List<Gem> gems=theQuery.getResultList();
		System.out.println(gems);

		return gems;
	}

	//delete Gem query
	@Override
	public void deleteGem(long id) {

		Session currentSession=sessionFactoryGem.getCurrentSession();
		Gem gem=(Gem) currentSession.load(Gem.class,new Long(id));
        if(null !=gem){
        	currentSession.delete(gem);
        }
        System.out.println("Gem deleted successfully, Dept Details ="+gem);
	}

	//add gem query
	@Override
	public void addGem(Gem g) {

		Session currentSession=sessionFactoryGem.getCurrentSession();
		
        if(null !=g){
        	currentSession.save(g);
            System.out.println("Gem added successfully, Gem Details ="+g);

        }
        else{
        	
        	System.out.println("Null Gem!!!");
        }
	}

	@Override
	public void updateGem(Gem g) {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		
        	currentSession.update(g);

	}

	@Override
	public Gem getGem(long id) {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		Gem gem= currentSession.get(Gem.class,id);
		if(null !=gem){
			System.out.println("Gem  get By id, Gem Details ="+gem);
			}
		else{
			System.out.println("Gem could not found ="+gem);

		}
		return gem;
	}

}
