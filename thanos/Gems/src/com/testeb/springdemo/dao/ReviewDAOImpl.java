package com.testeb.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.testeb.springdemo.entity.Gem;
import com.testeb.springdemo.entity.Review;

@Repository
public class ReviewDAOImpl implements ReviewDAO {

	@Autowired
	private SessionFactory sessionFactoryGem;//ειναι ίδιο με το id του, στο xml

	
	@Override
	public List<Review> getReviews() {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		Query<Review> theQuery =currentSession.createQuery("from Review",Review.class);
		//execute query and get result list
		List<Review> reviews=theQuery.getResultList();
		//System.out.println(reviews);

		return reviews;

	}

	@Override
	public void deleteReview(long id) {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		Review review=(Review) currentSession.load(Review.class,new Long(id));
        if(null !=review){
        	currentSession.delete(review);
        }
        System.out.println("Review deleted successfully, Review Details ="+review);
	}

	@Override
	public void addReview(Review r) {

		Session currentSession=sessionFactoryGem.getCurrentSession();
		
        	currentSession.save(r);
	}

	@Override
	public void updateReview(Review r) {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		
    	currentSession.update(r);
	}

	@Override
	public Review getReview(long id) {
		Session currentSession=sessionFactoryGem.getCurrentSession();
		Review review= currentSession.get(Review.class,id);
		if(null !=review){
			System.out.println("Review  get By id, Review Details ="+review);
			}
		else{
			System.out.println("Review could not found ="+review);

		}
		return review;

	}

}
