package com.testeb.springdemo.dao;

import java.util.List;

import com.testeb.springdemo.entity.Gem;

public interface GemDAO {

	public List<Gem> getGems();
	public void deleteGem(long id); 
	public void addGem(Gem c);
	public void updateGem(Gem c);
	public Gem getGem(long id);
}
