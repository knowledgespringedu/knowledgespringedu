package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.testeb.springdemo.dao.CustomerDAO;
import com.testeb.springdemo.entity.Customer;
import com.testeb.springdemo.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	//need to inject the DAO 
	//@Autowired
	//private CustomerDAO customerDAO;
	
	
	//now need to inject service layer
	@Autowired
	private CustomerService customerService;
	
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel){
		Customer person=new Customer();		
		theModel.addAttribute("person",person);
		return"addForm";
	}
	@GetMapping("/showFormForUpdate/{id}")
	public String showFormForUpdate(@PathVariable("id") int id,Model theModel){
		
		Customer person=customerService.getCustomerWithId(id);
		theModel.addAttribute("person",person);
		return"updateForm";
		
	}
	
	
	
	@GetMapping("/list")
	public String showList(Model theModel){
		
		//get customers from DAO
		List<Customer> theCustomers = customerService.getCustomers();
		
		
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		
		
		
		
		
		return "list-customer";
	}
	@GetMapping("/delete/{id}")
	public String deleteFromList(@PathVariable("id") int id){
		
		 customerService.deleteCustomer(id);
		
		
		
		return "redirect:/customer/list";
	}
	
	 @RequestMapping(value = "/add",method = RequestMethod.POST)
	 public String addCustomer(@ModelAttribute("person")Customer c){
		customerService.addCustomer(c);
		
		return "redirect:/customer/list";
	}
	 
	 
	 @RequestMapping(value = "/update",method = RequestMethod.POST)
	 public String updateCustomer(@ModelAttribute("person")Customer c){
		customerService.updateCustomer(c);
		
		return "redirect:/customer/list";
	}
	 
}
