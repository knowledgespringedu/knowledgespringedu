package com.testeb.springdemo.dao;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerDAO {

	public List<Customer> getCustomers();
	public void deleteCustomer(int id); 
	public void addCustomer(Customer c);
	public void updateCustomer(Customer c);
	public Customer getCustomer(int id);
}
