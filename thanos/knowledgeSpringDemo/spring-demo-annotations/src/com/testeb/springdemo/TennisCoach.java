package com.testeb.springdemo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("thatSillyCoach")//if i wanted i could leave empty id field and then i can have tennisCoach as id 
@Scope("prototype")
public class TennisCoach implements Coach {

	@Value("$foo.email")
	private String email;
	@Autowired
	@Qualifier("BFortuneService")
	private FortuneService fortuneService;
	
	public TennisCoach(){
		
	}
	
	@PostConstruct
	public void domyInit(){
		
		
	}
	
	@PreDestroy
	public void domyDestroy(){
		System.out.println(email);
		
	}
	/*@Autowired
	public TennisCoach(FortuneService theFortuneService){
		fortuneService=theFortuneService;
	}*/
	
	
	//setter method
	/*@Autowired
	public void setFortuneService(FortuneService theFortuneService){
		System.out.println("TennisCoach: > inside setter method fortuneservice");
		fortuneService=theFortuneService;
	}
	*/
	
	@Override
	public String getDailyWorkout() {
		return "Practice your backhand!";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
