package com.testeb.springdemo;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestCoach {

	@Test
	public void testCreateCoach()throws Exception{
		ClassPathXmlApplicationContext context=
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//get the bean from spring container
		
		Coach theCoach=context.getBean("thatSillyCoach",Coach.class);
		//call a method on the bean
		
		System.out.println(theCoach.getDailyWorkout());
		
	}
}
