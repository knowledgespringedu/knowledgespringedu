package com.testeb.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.testeb.hibernate.demo.entity.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) {

		//create session factory
		SessionFactory factory=new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		//create a session
		Session session=factory.getCurrentSession();
		
		
		//
		try{
			int studentId=4;
			
			//now get a new session and start transaction
			session =factory.getCurrentSession();
			session.beginTransaction();
			//retrieve student based on the id: primary key
			System.out.println("\nGetting student with id: "+studentId);
			
			Student myStudent= session.get(Student.class, studentId);
			System.out.println("GET complete: "+ myStudent);
			
			System.out.println("\n Deleting student.. ");
			session.delete(myStudent);
			
			session.createQuery("delete from Student where id=2").executeUpdate();


			//commit the transaction
			session.getTransaction().commit();
			
			
			
			System.out.println("DONE!!");

			
		}
		finally{
			factory.close();
		}
		
		
		
	}

}
