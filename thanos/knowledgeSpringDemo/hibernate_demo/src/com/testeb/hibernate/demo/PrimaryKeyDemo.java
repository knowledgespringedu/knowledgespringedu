package com.testeb.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.testeb.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {


	public static void main(String[] args) {

		//create session factory
		SessionFactory factory=new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		//create a session
		Session session=factory.getCurrentSession();
		
		
		//
		try{
			
			//create a studen object
			System.out.println("Creating 3 new student object...");
			Student tempStudent1= new Student("Thanos1","Testempasis1","ttestempasis1@knowledge.gr");
			Student tempStudent2= new Student("Thanos2","Testempasis2","ttestempasis2@knowledge.gr");
			Student tempStudent3= new Student("Thanos3","Testempasis3","ttestempasis3@knowledge.gr");

			//start a transaction
			session.beginTransaction();
			
			//save the sudent object
			System.out.println("Saving the student...");
			session.save(tempStudent1);
			session.save(tempStudent2);
			session.save(tempStudent3);

			//commit transaction
			session.getTransaction().commit();
			System.out.println("Done!!");
		}
		finally{
			factory.close();
		}
		
		
		
	}

}
