package com.testeb.springdemo.dao;

import java.util.List;

import com.testeb.springdemo.entity.Customer;

public interface CustomerDAO {

	public List<Customer> getCustomers();
}
