package com.testeb.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.testeb.springdemo.dao.CustomerDAO;
import com.testeb.springdemo.entity.Customer;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	//need to inject the DAO 
	@Autowired
	private CustomerDAO customerDAO;
	
	
	
	@RequestMapping("/list")
	public String showList(Model theModel){
		
		//get customers from DAO
		List<Customer> theCustomers = customerDAO.getCustomers();
		
		//add the customers to the model
		theModel.addAttribute("customers",theCustomers);
		
		
		
		return "list-customer";
	}
}
