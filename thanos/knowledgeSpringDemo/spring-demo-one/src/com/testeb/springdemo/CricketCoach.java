package com.testeb.springdemo;

public class CricketCoach implements Coach {

	private FortuneService fortuneService;
	
	//add new field for email
	private String emailAdress;
	private String team;
	
	//create ano-arg constractor
	public CricketCoach(){
		System.out.println("CricketCoach: inside no-arg constactor");
	}
	
	//getters and setter for emailAdress an team
	public String getEmailAdress() {
		return emailAdress;
	}


	public void setEmailAdress(String emailAdress) {
		System.out.println("CricketCoach: setter method - emailAdress");

		this.emailAdress = emailAdress;
	}


	public String getTeam() {
		return team;
	}


	public void setTeam(String team) {
		System.out.println("CricketCoach: setter method - team");

		this.team = team;
	}


	//setter method
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("CricketCoach: setter method - setFortuneService");
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Pactice fast bowling for 15 minutes";
	}

	@Override
	public String getDailyFortue() {
		return fortuneService.getFortune();
	}

}
