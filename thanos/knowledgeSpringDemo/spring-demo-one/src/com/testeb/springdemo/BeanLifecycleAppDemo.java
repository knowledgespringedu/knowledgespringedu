package com.testeb.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanLifecycleAppDemo {

	public static void main(String[] args) {
		
		//load spring config class
				ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean_lifecycle_applicationContext.xml");
				
				//retrieve bean from spring container
				Coach theCoach= context.getBean("myCoach",Coach.class);
				
				
				//call methods on bean
				
				System.out.println(theCoach.getDailyWorkout());
				
				
				// call the method for fortunes
				System.out.println(theCoach.getDailyFortue());
				
				//close the context
				context.close();
		
		
		
		
	}

}
