package com.testeb.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {

	public static void main(String[] args) {

		//load spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		//retrieve the bean from bean container
		CricketCoach theCoach=context.getBean("myCricketCoach",CricketCoach.class);
		
		//call methods on the bean
		
		System.out.println(theCoach.getDailyWorkout());
		System.out.println(theCoach.getDailyFortue());
		System.out.println(theCoach.getEmailAdress());
		System.out.println(theCoach.getTeam());


		//close the context
		context.close();
	}

}
