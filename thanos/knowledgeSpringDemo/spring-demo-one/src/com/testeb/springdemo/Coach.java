package com.testeb.springdemo;

public interface Coach {
	public String getDailyWorkout();
	public String getDailyFortue();
}
