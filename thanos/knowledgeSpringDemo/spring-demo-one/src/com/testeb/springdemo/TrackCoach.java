package com.testeb.springdemo;

public class TrackCoach implements Coach {

	//define a private field for the dependency
	private FortuneService fortuneService;
		
		
	public TrackCoach(){
			
	}
		
	public TrackCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Run a hard 5k";
	}

	@Override
	public String getDailyFortue() {
		return "Just do it: " + fortuneService.getFortune();

	}

	//add init method
	public void doMyStartAppStuff(){
		System.out.println("TrackCoach:inside doMyStartAppStuff");
	}
	
	//add destroy method
	public void doMyCleanAppStuff(){
		System.out.println("TrackCoach:inside doMyCleanAppStuff");
	}
	
}
