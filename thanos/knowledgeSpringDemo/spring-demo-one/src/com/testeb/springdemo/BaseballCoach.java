package com.testeb.springdemo;

public class BaseballCoach implements Coach{

	
	//define a private field for the dependency
	private FortuneService fortuneService;
	
	//define a contractor for the dependency injection
	public BaseballCoach(FortuneService theFortuneService){
		fortuneService=theFortuneService;
	}
	
	
	@Override
	public String getDailyWorkout(){
		return "Spend 30 minutes on batting practice";
	}

	@Override
	public String getDailyFortue() {
		// use my fortuneService to get a fortune
		return fortuneService.getFortune();
	}
}
