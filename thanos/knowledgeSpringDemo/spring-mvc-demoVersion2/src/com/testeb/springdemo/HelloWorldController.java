package com.testeb.springdemo;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {
	
	@RequestMapping("/showForm")
	public String showForm(){
		// helloworld-form  is the page view name (web.xml : jsp)
		return "helloworld-form";
	}
	
	@RequestMapping("/processForm")
	public String processForm(){
		return "helloworld";
	}
	
	// new controller method to read form data and 
	//add data to the model
	@RequestMapping("/processFormTwo")
	public String letsShout(HttpServletRequest request,Model model){
		
		//i can do ::  public String letsShout(@RequestParam("studentName") String theName,Model model){} and delete line32
		//example /processFormThree below
		
		//read the request parameter from the HTML form
		String theName=request.getParameter("studentName");
		
		//convert the data to all caps
		theName=theName.toUpperCase();
		
		//create a message
		String result="YO! "+theName;
		
		//add message to the model
		
		model.addAttribute("message", result);
		
		return"helloworld";
	}
	
	
	@RequestMapping("/processFormThree")
	public String letsShout(@RequestParam("studentName") String theName,Model model){		
				
		//convert the data to all caps
		theName=theName.toUpperCase();
		
		//create a message
		String result="form three message : "+theName;
		
		//add message to the model
		
		model.addAttribute("message", result);
		
		return"helloworld";
	}
	
	

}
